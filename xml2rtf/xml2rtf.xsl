<!--
Converts valid XML to RTf
-->
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0"
    xmlns:rtf="http://rtf2xml.sourceforge.net/"
>
<xsl:output method = "text"/>

<xsl:key name = "font-numbers" match = "rtf:font-in-table" use = "@name"/>

<xsl:key name = "style-numbers" match = "rtf:paragraph-style-in-table"
    use = "@name"/>

<xsl:key name = "color-numbers" match = "rtf:color-in-table" use = "@value"/>

<!--The root element doc needs an opening and closing bracket.
It also needs a code page. I use 1252 for convenience's sake
-->
<xsl:template match = "rtf:doc">
    <xsl:text>{\rtf1\ansi\ansicpg1252\uc1&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#xA;}</xsl:text>
</xsl:template>

<!--The font table requires an opening and closing bracket and the
words \fonttbl
-->
<xsl:template match = "rtf:font-table">
    <xsl:text>{\fonttbl&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}&#xA;</xsl:text>
</xsl:template>


<!--
<font-in-table name="Times" num="4"/>

font-in-table requires an opening and closing bracket.
The final result should look like:
{\f1 Times;}
-->

<xsl:template match = "rtf:font-in-table">
    <xsl:text>{\f</xsl:text>
    <xsl:value-of select = "@num"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select = "@name"/>
    <xsl:text>;&#xA;</xsl:text>
    
</xsl:template>

<!--
<color-table>

The final result should look like this:
{\colortbl; [individual colors] }}
-->
<xsl:template match = "rtf:color-table">
    <xsl:text>{\colortbl; &#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#xA;}&#xA;</xsl:text>
</xsl:template>


<!--
<color-in-table num="1" value="#000000"/>

The final result should look like:
\red0\green0\blue0;


-->

<xsl:template match = "rtf:color-in-table">
    <xsl:text>\red</xsl:text>
    <xsl:call-template name = "convert-hex">
        <xsl:with-param name = "num" select = "substring(@value,2,2)"/>
    </xsl:call-template>
    <xsl:text>\green</xsl:text>
    <xsl:call-template name = "convert-hex">
        <xsl:with-param name = "num" select = "substring(@value,4,2)"/>
    </xsl:call-template>
    <xsl:text>\blue</xsl:text>
    <xsl:call-template name = "convert-hex">
        <xsl:with-param name = "num" select = "substring(@value,6,2)"/>
    </xsl:call-template>
    <xsl:text>;&#xA;</xsl:text>
</xsl:template>

<!--
<style-table>

Should look like:

{\stylesheet
[individual styles]
}

-->

<xsl:template match = "rtf:style-table">
    <xsl:text>{\stylesheet</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}&#xA;</xsl:text>
</xsl:template>

<!--
<paragraph-style-in-table num="0" nest-level="0" language="1033" adjust-right="true" next-style="Normal" widow-control="true" font-style="Times" name="Normal"/>

Should look like:
{\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \f4\lang1033\cgrid \snext0 Normal;}
-->

<xsl:template match = "rtf:paragraph-style-in-table">
    <xsl:text>&#xA;{\s</xsl:text>
    <xsl:value-of select = "@num"/>

    <xsl:call-template name = "paragraph-info"/>
    <xsl:call-template name = "character-info"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select = "@name"/>
    <xsl:text>;}&#xA;</xsl:text>
</xsl:template>

<!--
Template converts character info to RTF.

<inline hidden = "true"> => \v
<paragraph-in-table hidden = "true"> => \v
-->

<xsl:template name = "paragraph-info">

    <xsl:if test = "@nest-level">
        <xsl:text>\itap</xsl:text>
        <xsl:value-of select = "@nest-level"/>
    </xsl:if>
    <xsl:if test = "@widow-control = 'true'">
        <xsl:text>\widctlpar</xsl:text>
    </xsl:if>
    <xsl:if test = "@widow-control = 'false'">
        <xsl:text>\nowidctlpar</xsl:text>
    </xsl:if>
    <xsl:if test = "@adjust-right">
        <xsl:text>\adjustright</xsl:text>
    </xsl:if>
    <xsl:if test = "@language">
        <xsl:text>\lang</xsl:text>
        <xsl:value-of select = "@language"/>
    </xsl:if>
    <xsl:if test = "@right-indent">
        <xsl:text>\ri</xsl:text>
        <xsl:value-of select = "@right-indent * 20"/>
    </xsl:if>
    <xsl:if test = "@left-indent">
        <xsl:text>\li</xsl:text>
        <xsl:value-of select = "@left-indent * 20"/>
    </xsl:if>
    <xsl:if test = "@first-line-indent">
        <xsl:text>\if</xsl:text>
        <xsl:value-of select = "@first-line-indent * 20"/>
    </xsl:if>
    <xsl:if test = "@space-before">
       <xsl:text>\sb</xsl:text> 
       <xsl:value-of select = "@space-before * 20"/>
    </xsl:if>
    <xsl:if test = "@space-after">
        <xsl:text>\sa</xsl:text>
        <xsl:value-of select = "@space-after * 20"/>
    </xsl:if>
    <xsl:if test = "@line-spacing">
        <xsl:text>\sl</xsl:text>
        <xsl:value-of select = "@line-spacing * 20"/>
    </xsl:if>
    <xsl:if test = "@align = 'left'">
        <xsl:text>\ql</xsl:text>
    </xsl:if>
    <xsl:if test = "@align = 'right'">
        <xsl:text>\qr</xsl:text>
    </xsl:if>
    <xsl:if test = "@align = 'center'">
        <xsl:text>\qc</xsl:text>
    </xsl:if>
    <xsl:if test = "@align = 'justified'">
        <xsl:text>\qj</xsl:text>
    </xsl:if>
    <xsl:if test = "@based-on-style">
        <xsl:text>\sbasedon</xsl:text>
        <xsl:value-of select = "key('style-numbers', @based-on-style)/@num"/>
    </xsl:if>
    <xsl:if test = "@next-style">
        <xsl:text>\snext</xsl:text>
        <xsl:value-of select = "key('style-numbers', @next-style)/@num"/>
    </xsl:if>
    
</xsl:template>


<xsl:template name = "character-info">
    <xsl:param name = "inline">0</xsl:param>
    <xsl:if test = "@hidden">
        <xsl:text>\v</xsl:text>
    </xsl:if>
    <xsl:if test = "@bold='true'">
        <xsl:text>\b</xsl:text>
    </xsl:if>
    <xsl:if test = "@bold = 'false'">
        <xsl:text>\b0</xsl:text>
    </xsl:if>
    <xsl:if test = "@italics = 'true'">
        <xsl:text>\i</xsl:text>
    </xsl:if>
    <xsl:if test = "@italics = 'false'">
        <xsl:text>\i0</xsl:text>
    </xsl:if>
    <xsl:if test = "@strike-through = 'true'">
        <xsl:text>\strike</xsl:text>
    </xsl:if>
    <xsl:if test = "@shadow = 'true'">
        <xsl:text>\shad</xsl:text>
    </xsl:if>
    <xsl:if test = "@outline = 'true'">
        <xsl:text>\outl</xsl:text>
    </xsl:if>
    <xsl:if test = "@caps = 'true'">
        <xsl:text>\caps</xsl:text>
    </xsl:if>
    <xsl:if test = "@small-caps = 'true'">
        <xsl:text>\scaps</xsl:text>
    </xsl:if>
    <xsl:if test = "@double-strike-through = 'true'">
        <xsl:text>\striked</xsl:text>
    </xsl:if>
    <xsl:if test = "@font-color">
        <xsl:text>\cf</xsl:text>
        <xsl:value-of select = "key('color-numbers', @font-color)/@num"/>
    </xsl:if>
    <xsl:if test = "@font-style">
        <xsl:text>\f</xsl:text>
        <xsl:value-of select = "key('font-numbers', @font-style)/@num"/>
    </xsl:if>
    <xsl:if test = "@font-size">
       <xsl:text>\fs</xsl:text> 
       <xsl:value-of select = "@font-size * 2"/>
    </xsl:if>
    <xsl:if test = "@superscript">
        <xsl:text>\up</xsl:text>
        <xsl:value-of select = "@superscript * 2"/>
    </xsl:if>
    <xsl:if test = "@subscript">
        <xsl:text>\dn</xsl:text>
        <xsl:value-of select = "@subscript * 2"/>
    </xsl:if>
    <xsl:if test = "@plain = 'true'">
        <xsl:text>\plain</xsl:text>
    </xsl:if>
    <xsl:if test = "@emboss = 'true'">
        <xsl:text>\embo</xsl:text>
    </xsl:if>
    <xsl:if test = "@engrave = 'true'">
        <xsl:text>\impr</xsl:text>
    </xsl:if>
    <xsl:if test = "@underline">
        <xsl:call-template name = "underline"/>
    </xsl:if>


    
</xsl:template>

<xsl:template name = "underline">
    
</xsl:template>

<!--


        'emboss____'    :	'emboss',
        'engrave___'    :	'engrave',
        'red_______'    :	'red',
        'blue______'    :	'blue',
        'green_____'    :	'green',




        'blue'               :	('ci', 'blue______', self.color_func),
        'chftn'              :	('ci', 'footnot-mk', self.bool_st_func),
        'green'              :	('ci', 'green_____', self.color_func),
        'red'                :	('ci', 'red_______', self.color_func),

default-tab         =>          \deftab



-->

<!--

        'tx'                 :  ('pf', 'tab-stop__', self.divide_by_20),
        'tb'                 :  ('pf', 'tab-bar-st', self.divide_by_20),
        'tqr'                :  ('pf', 'tab-right_', self.default_func),
        'tqdec'              :  ('pf', 'tab-dec___', self.default_func),
        'tqc'                :  ('pf', 'tab-center', self.default_func),
        'tlul'               :  ('pf', 'leader-und', self.default_func),
        'tlhyph'             :  ('pf', 'leader-hyp', self.default_func),
        'tldot'            


        'cs'                 :	('ss', 'char-style', self.default_func),
-->

<!--


        

        'based-on__'    :	'based-on-style',
        'next-style'    :	'next-style',
        'char-style'    :	'character-style',
        'para-style'    :	'paragraph-style',


        # list=> ls
        'list-text_'    :	'list-text',

        # this line must be wrong because it duplicates an earlier one
        'list-text_'    :	'list-text',
        'list______'    :	'list',
        'list-lev-d'    :	'list-level-definition',
        'list-cardi'    :	'list-cardinal-numbering',
        'list-decim'    :	'list-decimal-numbering',
        'list-up-al'    :	'list-uppercase-alphabetic-numbering',
        'list-up-ro'    :	'list-uppercae-roman-numbering',
        'list-ord__'    :	'list-ordinal-numbering',
        'list-ordte'    :	'list-ordinal-text-numbering',
        'list-bulli'    :	'list-bullet',
        'list-simpi'    :	'list-simple',
        'list-conti'    :	'list-continue',
        'list-hang_'    :	'list-hang',
        # 'list-tebef'    :	'list-text-before',
        'list-level'    :	'level',
        'list-id___'    :	'list-id',
        'list-start'    :	'list-start',
        'nest-level'    :	'nest-level',

        # duplicate
        'list-level'    :	'list-level',


        # notes => nt
        'footnote__'    :	'footnote',
        'type______'    :	'type',

        # anchor => an
        'toc_______'    :	'anchor-toc',
        'book-mk-st'    :	'bookmark-start',
        'book-mk-en'    :	'bookmark-end',
        'index-mark'    :	'anchor-index',
        'place_____'    :	'place',


        # character info => ci

        # table => tb
        'row-def___'    :	'row-definition',
        'cell______'    :	'cell',
        'row_______'    :	'row',
        'in-table__'    :	'in-table',
        'columns___'    :	'columns',
        'row-pos-le'    :	'row-position-left',
        'cell-posit'    :	'cell-position',

        # preamble => pr



        # underline
        'underlined'    :	'underlined',

        # border => bd
        'bor-t-r-hi'    :	'border-table-row-horizontal-inside',
        'bor-t-r-vi'    :	'border-table-row-vertical-inside',
        'bor-t-r-to'    :	'border-table-row-top',
        'bor-t-r-le'    :	'border-table-row-left',
        'bor-t-r-bo'    :	'border-table-row-bottom',
        'bor-t-r-ri'    :	'border-table-row-right',
        'bor-cel-bo'    :	'border-cell-bottom',
        'bor-cel-to'    :	'border-cell-top',
        'bor-cel-le'    :	'border-cell-left',
        'bor-cel-ri'    :	'border-cell-right',
        'bor-par-bo'    :	'border-paragraph-bottom',
        'bor-par-to'    :	'border-paragraph-top',
        'bor-par-le'    :	'border-paragraph-left',
        'bor-par-ri'    :	'border-paragraph-right',
        'bor-par-bo'    :	'border-paragraph-box',
        'bor-for-ev'    :	'border-for-every-paragraph',
        'bor-outsid'    :	'border-outisde',
        'bor-none__'    :	'border',

        # border type => bt
        'bdr-single'    :	'single',
        'bdr-doubtb'    :	'double-thickness-border',
        'bdr-shadow'    :	'shadowed-border',
        'bdr-double'    :	'double-border',
        'bdr-dotted'    :	'dotted-border',
        'bdr-dashed'    :	'dashed',
        'bdr-hair__'    :	'hairline',
        'bdr-inset_'    :	'inset',
        'bdr-das-sm'    :	'dash-small',
        'bdr-dot-sm'    :	'dot-dash',
        'bdr-dot-do'    :	'dot-dot-dash',
        'bdr-outset'    :	'outset',
        'bdr-trippl'    :	'tripple',
        'bdr-thsm__'    :	'thick-thin-small',
        'bdr-htsm__'    :	'thin-thick-small',
        'bdr-hthsm_'    :	'thin-thick-thin-small',
        'bdr-thm__'     :	'thick-thin-medium',
        'bdr-htm__'     :	'thin-thick-medium',
        'bdr-hthm_'     :	'thin-thick-thin-medium',
        'bdr-thl__'     :	'thick-thin-large',
        'bdr-hthl_'     :	'think-thick-think-large',
        'bdr-wavy_'     :	'wavy',
        'bdr-d-wav'     :	'double-wavy',
        'bdr-strip'     :	'striped',
        'bdr-embos'     :	'emboss',
        'bdr-engra'     :	'engrave',
        'bdr-frame'     :	'frame',
        'bdr-li-wid'    :	'line-width',

        # tabs
        'tab-center'  :   'center',
        'tab-right_'  :   'right',
        'tab-dec___'  :   'decimal',
        'leader-dot'  :   'leader-dot',
        'leader-hyp'  :   'leader-hyphen',
        'leader-und'  :   'leader-underline',

        }

-->


<!--
Template used to convert two-digit hexidecimals to base-10 numbers.
First get the ones digit and use the hex-to-dec to convert this to 
a base-10 number.

Get the first digit and use the hex-to-dec to convert this to 
a base-10 number. Multiply this number by 16. 

Add the ones and the tens variable to get the number

-->
<xsl:template name = "convert-hex">
    <xsl:param name = "num">00</xsl:param>
    <xsl:variable name = "ones">
        <xsl:call-template name = "hex-to-dec">
        <xsl:with-param name = "digit" select = "substring($num, 2, 1)"/>
        </xsl:call-template>
    </xsl:variable>

    <xsl:variable name = "tens">
        <xsl:variable name = "tens-digit">
            <xsl:call-template name = "hex-to-dec">
            <xsl:with-param name = "digit" select = "substring($num, 1, 1)"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select = "$tens-digit * 16"/>
    </xsl:variable>
    <xsl:value-of select = "$ones + $tens"/>
</xsl:template>


<!--

Template converts one hexidecimal to a base-10 number.
-->


<xsl:template name = "hex-to-dec">
    <xsl:param name = "digit">0</xsl:param>
    <xsl:choose>
        <xsl:when test = "$digit = '0'">
            <xsl:text>0</xsl:text>
        </xsl:when>
        <xsl:when test = "$digit = '1'">
            <xsl:text>1</xsl:text>
        </xsl:when>
        <xsl:when test = "$digit = '2'">
            <xsl:text>2</xsl:text>
        </xsl:when>
        <xsl:when test = "$digit = '3'">
            <xsl:text>3</xsl:text>
        </xsl:when>
        <xsl:when test = "$digit = '4'">
            <xsl:text>4</xsl:text>
        </xsl:when>
        <xsl:when test = "$digit = '5'">
            <xsl:text>5</xsl:text>
        </xsl:when>
        <xsl:when test = "$digit = '6'">
            <xsl:text>6</xsl:text>
        </xsl:when>
        <xsl:when test = "$digit = '7'">
            <xsl:text>7</xsl:text>
        </xsl:when>
        <xsl:when test = "$digit = '8'">
            <xsl:text>8</xsl:text>
        </xsl:when>
        <xsl:when test = "$digit = '9'">
            <xsl:text>9</xsl:text>
        </xsl:when>
        <xsl:when test = "$digit = 'A'">
            <xsl:text>10</xsl:text>
        </xsl:when>
        <xsl:when test = "$digit = 'B'">
            <xsl:text>11</xsl:text>
        </xsl:when>
        <xsl:when test = "$digit = 'C'">
            <xsl:text>12</xsl:text>
        </xsl:when>
        <xsl:when test = "$digit = 'D'">
            <xsl:text>13</xsl:text>
        </xsl:when>
        <xsl:when test = "$digit = 'E'">
            <xsl:text>14</xsl:text>
        </xsl:when>
        <xsl:when test = "$digit = 'F'">
            <xsl:text>15</xsl:text>
        </xsl:when>
    </xsl:choose>
</xsl:template>



</xsl:stylesheet>
