#!/usr/bin/env python
"""

move old files to old folder

"""
import os, sys

class MoveFiles:

    def __init__(self, move_to_dir, the_file):
        self.__move_to_dir = move_to_dir
        self.__the_file = the_file
        
        pass

    def move_files(self):
        read_obj = open(self.__the_file, 'r')
        line = 1
        while line:
            line = read_obj.readline()
            line = line.replace('\n', '')
            if not os.path.isfile(line):
                sys.stderr.write('Can\'t move %s: not a file\n' % line)
                continue
            base_name = os.path.basename(line)
            move_to = os.path.join(self.__move_to_dir, base_name)
            if os.path.isfile(move_to):
                sys.stdout.write('"%s" already exists in "%s"' 
                        % (move_to, self.__move_to_dir))
                continue
            command = 'mv %s %s' % (line, move_to)
            os.system(command)


if __name__ == '__main__':
    this_file = sys.argv[1]
    the_move_to_dir = '/home/paul/Documents/data/rtf2xml_test/old_rtf'
    the_move_to_dir = '/home/paul/tmp/old'
    move_obj = MoveFiles(
            move_to_dir = the_move_to_dir,
            the_file = this_file,
            )
    move_obj.move_files()
        
