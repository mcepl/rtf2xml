#!/usr/bin/env python

import sys, os

"""

script for adding the many border attributes


"""


# not used anymore
non_border_para_atts = \
"""


tabs-right			CDATA		#IMPLIED
tabs-left			CDATA		#IMPLIED
tabs-center			CDATA		#IMPLIED
tabs-decimal			CDATA		#IMPLIED
tabs-bar			CDATA		#IMPLIED
adjust-right			CDATA		#IMPLIED
align				CDATA		#IMPLIED
first-line-indent		CDATA		#IMPLIED
gutter				CDATA 		#IMPLIED
keep-with-next			CDATA		#IMPLIED
left-indent			CDATA		#IMPLIED
level				CDATA		#IMPLIED
line-spacing			CDATA		#IMPLIED
list				CDATA		#IMPLIED
list-decimal-numbering		CDATA	 	#IMPLIED
list-hang			CDATA		#IMPLIED
list-id				CDATA		#IMPLIED
list-level			CDATA		#IMPLIED
list-simple			CDATA		#IMPLIED
list-start			CDATA		#IMPLIED
margin-bottom			CDATA 		#IMPLIED
margin-left			CDATA 		#IMPLIED
margin-right			CDATA 		#IMPLIED
margin-top			CDATA 		#IMPLIED
name				CDATA		#REQUIRED
nest-level			CDATA		#IMPLIED
next-style			CDATA		#IMPLIED
right-indent			CDATA		#IMPLIED
space-after			CDATA		#IMPLIED
space-before			CDATA		#IMPLIED
widow-control			(true|false)	#IMPLIED
"""

non_border_row_atts = \
"""
number-of-cells             CDATA           #IMPLIED
left-row-position           CDATA           #IMPLIED
widths                      CDATA           #IMPLIED
header                      CDATA           #IMPLIED
border                      (none)          #IMPLIED

"""
non_border_cell_atts = \
"""
width                       CDATA           #IMPLIED
border                      (none)          #IMPLIED

"""



block_types = ['paragraph', 'table-row', 'cell']

# also there is a place called outer. See border_parse.py. Not 
# sure what to do with outer
places = ['top', 'bottom', 'right', 'left',  'box', 'vertical-inside', 
    'horizontal-inside', 'border-for-every-paragraph', ] 
                         # 'border-for-every-paragraph-line-width

descriptions = [
'line-width	CDATA	#IMPLIED',
'padding	CDATA	#IMPLIED',
'color  	CDATA	#IMPLIED',
'style	    CDATA	#IMPLIED',

]




for type in block_types:
    if type == 'paragraph':
        sys.stdout.write('<!ENTITY % paragraph-border-styles "\n')
        # sys.stdout.write(non_border_para_atts )
    elif type == 'table-row':
        sys.stdout.write('">\n\n\n')
        sys.stdout.write('<!ENTITY % row-attributes " \n')
        sys.stdout.write(non_border_row_atts)
    elif type == 'cell':
        sys.stdout.write('">\n\n\n')
        sys.stdout.write('<!ENTITY % cell-attributes "\n')
        sys.stdout.write(non_border_cell_atts)
    for place in places:
        sys.stdout.write('border-%s-%s\tCDATA\t\t#IMPLIED\n' % 
                (type, place))
        for description in descriptions:
            if place == 'box' and type == 'table-row':
                pass
            elif place == 'box' and type == 'cell':
                pass
            elif place == 'border-for-every-paragraph' and type == 'cell':
                pass
            elif place == 'border-for-every-paragraph' and type == 'table-row':
                pass
            elif place == 'horizontal-inside' and type == 'paragraph':
                pass
            elif place == 'vertical-inside' and type == 'paragraph':
                pass
            elif place == 'horizontal-inside' and type == 'cell':
                pass
            elif place == 'vertical-inside' and type == 'cell':
                pass
            elif place == 'bottom_' and type == 'paragraph':
                pass
            else:
                the_string = 'border-%s-%s-%s\n' % (type, place, description)
                the_string = the_string.replace('border-paragraph-border-for-every-paragraph', 
                        'border-for-every-paragraph')

                
                sys.stdout.write(the_string)

sys.stdout.write('">\n\n\n')
    

