#!/usr/bin/env python

import os, sys

"""

Create a RTF file with number type fields

"""
header = 'rtf_head'

command = 'cat %s' % header

os.system(command)


field_names = [ 'EDITTIME',
                'NUMCHARS', 
                'NUMPAGES',
                'NUMWORDS',
                'REVNUM',
                'SECTIONPAGES',
                'SECTION',
                'QUOTE',
                
    
    
    ]

num_types = ['Arabic',
            'alphabetic',
            'ALPHABETIC',
            'roman',
            'ROMAN',
            'Ordinal' ,
            'CardText',
            'OrdText',
            'Hex',
            'DollarText',
            'Upper',
            'Lower',
            'FirstCap' ,
            'Caps',
            ]

for field_name in field_names:
    for num_type in num_types:
        sys.stdout.write('\par \n')
        sys.stdout.write('Text:')
        sys.stdout.write('{\\field{\*\\fldinst { ') 
        sys.stdout.write(field_name)
        sys.stdout.write(' \\\\* ')
        sys.stdout.write(num_type)
        sys.stdout.write(' \\\\* MERGEFORMAT }}{\\fldrslt {\lang1024 }}}{ ')
        sys.stdout.write('%s , %s' % (field_name, num_type))
        sys.stdout.write('}')
        sys.stdout.write(':text')
        sys.stdout.write('\n')
    
sys.stdout.write('}')


