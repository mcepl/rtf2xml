import os, sys, re

"""
Check a raw RTF file to see if brackets match


"""

class BracketsMatch:

    def __init__(self):
        self.__ob = 0
        self.__cb = 0


        
        
        
    def check_brackets(self, in_file):
        nums = [0, 1, 2, 3, 4, 5, 6, 7, 8,  11,  13, 14, 15, 16, 17, 18, 19]
        illegal_regx = re.compile( '\x00|\x01|\x02|\x03|\x04|\x05|\x06|\x07|\x08|\x0B|\x0E|\x0F|\x10|\x11|\x12|\x13')
        read_obj = open(in_file, 'r')
        out_file = '/home/paul/tmp/brackets_check.rtf'
        write_obj = open(out_file, 'w')
        line_to_read = 1
        while line_to_read:
            line_to_read = read_obj.readline()
            line = line_to_read
            line = line.replace('\\\\', '')
            line = line.replace('\\{', '')
            line = line.replace('\\}', '')
            try:
                line.decode('us-ascii')
            except UnicodeError, msg:
                sys.stderr.write('File not ascii encoded.\n')
            line = line.decode('utf-8')

            # line = re.sub(illegal_regx, '', line)
            for num in nums:
                line = line.replace(chr(num), '')

            ob = line.count('{')
            self.__ob += ob
            cb = line.count('}')
            self.__cb += cb
            write_obj.write(line)
        print 'open brackets are "%s"' % self.__ob
        print 'closed bracets are "%s"' % self.__cb


if __name__ == '__main__':
    the_file = sys.argv[1]
    brac_obj = BracketsMatch()
    brac_obj.check_brackets(the_file)
