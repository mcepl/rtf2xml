#!/usr/bin/env python
import os, sys, commands, shutil
import txt2xml.options_trem
import time
import tempfile
"""

Use:

type test_files.py --help for a help message.

All lists get stored in the self.__store_dir, which 
right now is /home/paul/tmp. So look there to see lists



"""

class TestFiles:

    def __init__(self):
        self.__initiate_values()

    def __initiate_values(self):
        self.__file_list = []
        self.__store_dir = '/home/paul/tmp'
        self.__store_dir = '/home/paul/tmp/test_rtf'
        if os.path.isdir(self.__store_dir):
            shutil.rmtree(self.__store_dir)
        os.mkdir(self.__store_dir)
        self.__exclude_dir = [
            '/home/paul/Documents/data/rtf2xml_test/tough',
            # '/home/paul/Documents/data/rtf2xml_test/invalid_rtf',
        ]
        self.__exclude_files = ['temp', 'temp2', 'temp3', 'temp4', 
        'test', 'test2', 'test3','test4', 'test5', 'test6',
        'test7', 'test8', 'test9',]
        self.__total_file_size = 0
        self.__file_count = 0
        self.__pass_file = os.path.join(self.__store_dir, 'passed.txt')
        self.__error_file = os.path.join(self.__store_dir, 'error.txt')
        self.__time_file = os.path.join(self.__store_dir, 'time.txt')
        self.__script_error = 0
        self.__mean_time = []
        self.__pass = 0
        self.__invalid = 0
        try:
            os.remove(self.__pass_file)
            os.remove(self.__error_file)
            os.remove(self.__time_file)
        except OSError, msg:
            pass

    def __size_func(self, size):
        size = float(size)
        ksize = int(size)
        if size < 1000000:
            size = round(size/1000)
            size = int(size)
            label = 'k'
        else:
            size = round(size/1000000, 1)
            label = 'M'
        final_string = '%s %s' % (size, label)
        return ksize, size, label, final_string
        
    def __path_size_func(self, path):
        size = os.path.getsize(path)
        size = float(size)
        ksize = round(size/1000)
        if size < 1000000:
            size = round(size/1000)
            size = int(size)
            label = 'k'
        else:
            size = round(size/1000000, 1)
            label = 'M'
        final_string = '%s %s' % (size, label)
        return ksize, size, label, final_string

    def __elapsed_time(self, start_time, end_time):
        total_time = (end_time - start_time)
        minutes, seconds = divmod(total_time, 60)
        seconds = round(seconds, 0)
        time_string =  '%s minutes %s seconds' % (minutes, seconds) 
        return total_time, seconds, minutes, time_string



    def __sort_list(self):
        list_dict = {}
        new_list = []
        for the_file in self.__file_list:
            ksize, size, label, final_string  = self.__path_size_func(the_file)
            ksize = "%015d" % ksize
            list_dict[the_file] = ksize
            new_list.append('%s%s' % (ksize, the_file))


        self.__file_list = []
        new_list.sort()
        for name in new_list:
            final = name[15:]
            self.__file_list.append(final)



    def __make_xml_file(self, the_file):
        file_name, ext = os.path.splitext(the_file)
        xml_file = os.path.basename(file_name)
        xml_file = xml_file + '.xml'
        xml_file = os.path.join(self.__store_dir, xml_file)
        return xml_file

    def __do_command(self, in_file, out_file):
        start_time = time.time()
        command = 'rtf2xml '
        if self.__format:
            final_out = out_file
            out_file = tempfile.mktemp()
        if self.__convert_headings:
            command += ' --headings-to-sections '
        if self.__group_styles:
            command += ' --group-styles '
        if self.__no_lists:
            command += ' --no-lists '
        if self.__level:
            command += ' --level %s' % self.__level
        command += ' -o  %s  %s' % (out_file,  in_file)
        (exit_status, out_text) = commands.getstatusoutput(command)
        end_time = time.time()
        gross_time, seconds, minutes, time_string =  self.__elapsed_time(start_time, end_time)
        if self.__format:
            self.__do_xslt_proc(in_file = out_file, out_file = final_out)
        return exit_status, out_text, gross_time, seconds, minutes, time_string

    def __do_xslt_proc(self, in_file, out_file):
        if self.__format == 'tei':
            stylesheet = '/home/paul/cvs/rtf2xml/xsl_stylesheets/tei/to_tei.xsl'
        elif self.__format == 'sdoc':
            stylesheet = '/home/paul/cvs/rtf2xml/xsl_stylesheets/to_sdoc.xsl'


        else:
            raise 'format for stylesheet not found\n'

        if not os.path.isfile(stylesheet):
            raise 'stylesheet "%s" not found\n' % stylesheet
        command = 'xsltproc --novalid %s %s > %s' % (stylesheet, in_file, out_file)
        (exit_status, out_text) = commands.getstatusoutput(command)
        try:
            os.remove(in_file)
        except OSError:
            pass
    
    def __execute_command(self):
        start_time = time.time()
        for the_file in self.__file_list:
            ksize, size, label, size_string = self. __path_size_func(the_file)
            self.__print_start_file(the_file, size_string)
            out_file = self.__make_xml_file(the_file)
            exit_status, out_text, gross_time, seconds, minutes, time_string = self.__do_command(
                in_file = the_file,
                out_file = out_file,
                    )
            self.__print_time_for_file(the_file, 
                    gross_time, seconds, minutes, time_string, ksize)
            if exit_status == 0:
                self.__is_valid(the_file, out_file,)
            else:
                self.__print_no_success_run(the_file, out_text)
            self.__print_summary()
            try:
                os.remove(out_file)
            except OSError, msg:
                pass
        end_time = time.time()
        gross_time, seconds, minutes, time_string =  self.__elapsed_time(start_time, end_time)
        self.__print_final_tally(gross_time, seconds, minutes, time_string)

    def __print_start_file(self, the_file, size_string):
        self.__file_count += 1
        msg = ('\n**************************\n')
        msg += ('Working on %s\n' % the_file)
        msg += ('(%s of %s)\n' % (self.__file_count, len(self.__file_list)))
        msg += ('size is %s\n' % size_string)
        sys.stdout.write(msg)
        self.__print_to_file(msg, self.__time_file)

    def __print_time_for_file(self, 
            the_file, 
            gross_time,
            seconds, 
            minutes, 
            time_string, 
            ksize
        ):
        msg = ( 
                'time to process file is  is %s minutes %s seconds' % 
                (minutes, seconds)
            )

        if ksize != 0: 
            time_per_kil = gross_time/ksize
            time_per_kil = round(time_per_kil, 1)
            msg += ( ' (%s seconds per kilobyte)\n' % (time_per_kil)) 
            self.__mean_time.append(time_per_kil)
        else:
            msg += ('\n')
        sys.stdout.write(msg)
        self.__print_to_file(msg, self.__time_file)


    def __print_to_file(self, msg, the_file):
        lines = msg.split('\n')
        for line in lines:
            line = line.replace('*', '\\*')
            line = line.replace('(', '\\(')
            line = line.replace(')', '\\)')
            line = line.replace("'", "\\'")
            line = line.replace('"', '\\"')
            line = line.replace('<', '\\<')
            line = line.replace('>', '\\>')
            command = 'echo %s >> %s' % (line, the_file)
            os.system(command)
    
    def __is_valid(self, in_file, xml_file):
        command = 'xmlvalid %s' % xml_file
        (exit_status, out_text) = commands.getstatusoutput(command)
        if exit_status == 0:
            self.__print_success_run(in_file)
        else:
            self.__print_invalid(in_file, out_text)
    
    def __print_success_run(self, the_file):

        msg = 'valid\n'
        sys.stdout.write(msg)
        msg += '**************************\n'
        msg += '%s\n\n' % the_file
        self.__print_to_file(msg, self.__pass_file)
        self.__pass += 1

    def __print_invalid(self, the_file, out_text):
        msg = ('invalid:\n' )
        msg += (out_text)
        msg += ('\n')
        self.__invalid += 1
        sys.stdout.write(msg)
        msg2 = '**************************\n'
        msg2 += '%s\n\n' % the_file
        msg = msg2 + msg
        self.__print_to_file(msg, self.__error_file)

    def __print_no_success_run(self, the_file, out_text):
        msg = ('\n')
        msg += ('Problems with script!\n')
        msg += out_text
        self.__script_error += 1
        sys.stderr.write(msg)
        msg2 = '**************************\n'
        msg2 += '%s\n\n' % the_file
        msg = msg2 + msg
        self.__print_to_file(msg, self.__error_file)

    def __print_summary(self):
        msg = 'passed: %s script failure: %s invalid: %s\n' % (
                self.__pass, self.__script_error, self.__invalid)
        sys.stdout.write(msg)
    
    def __print_final_tally(self, gross_time, seconds, minutes, time_string):
        msg = ('\n\n****************************************************\n')
        msg += ('FINAL TALLY\n')
        msg += ('****************************************************\n')
        msg += ( 'total time is %s minutes %s seconds\n' % (minutes, seconds)) 

        avg_time = round(gross_time/(self.__total_file_size/1000), 1) 
        msg += ('seconds per kilobyte: %s\n' % avg_time)
        time_per_file = round(gross_time/len(self.__file_list), 1) 
        msg += ('seconds per file: %s\n' % time_per_file)
        the_mean = self.__mean_time_func()
        msg += 'mean time per file is %s\n' % the_mean
        msg += ('passed: %s\nscript error: %s\ninvalid: %s\n\n' % 
                (self.__pass,  self.__script_error, self.__invalid))
        sys.stdout.write(msg)

    def __mean_time_func(self):
        if not self.__mean_time:
            return 0
        self.__mean_time.sort()
        length = len(self.__mean_time)
        the_result, the_remainder = divmod(length, 2)
        if the_remainder: # odd number
            middle = (length -1)/ 2
            return self.__mean_time[middle]
        else:# even
            high_middle = length/2
            low_middle = high_middle - 1
            avg = (self.__mean_time[high_middle] + self.__mean_time[low_middle])/2
            avg = round(avg, 1)
            return avg
            

    def __print_first_tally(self):
        msg = ('\n\n****************************************************\n')
        msg += ('Total number of files to process is %s\n' % len(self.__file_list))
        ksize, size, label, final_string = self.__size_func(self.__total_file_size)
        msg += ('Total size is %s  \n\n' % (final_string))
        msg += ('\n\n****************************************************\n')
        sys.stdout.write(msg)
            




    def __exclude_func(self, path):
        base_file = os.path.basename(path)
        file_name, ext = os.path.splitext(base_file)
        dir_name = os.path.dirname(path)
        if ext != '.rtf' and  os.path.isfile(path):
            return 1
        if file_name in self.__exclude_files:
            return 1
        if dir_name in self.__exclude_dir:
            return 1
        if path[-12:] == 'rtf_pict_dir':
            return 1
        if path[-17] == 'rtf2xml_debug_dir':
            return 1
        if self.__size:
            ksize, size, label, final_string = self.__path_size_func(path)
            if ksize > self.__size:
                return 1
        return 0
        
    def __make_file_list(self, the_dir):
        paths = os.listdir(the_dir)
        for path in paths:
            path = os.path.join(the_dir, path)
            to_exclude = self.__exclude_func(path)
            if to_exclude:
                sys.stdout.write('Excluding "%s"\n' % path)
                continue
            if os.path.isdir(path):
                self.__make_file_list(path)
            else:
                size = os.path.getsize(path)
                self.__total_file_size += size
                self.__file_list.append(path)
    

    def __print_help_message(self):
        sys.stdout.write(
"""
test_files2.py --help

test_files2.py  [ --size=<max-file-size> ] 
                [ --format=<raw | sdoc | tei > | -f=<raw | sdoc | tei> ]
                [ --headings-to-sections ]
                [ --group-styles ]
                [ --level ]
                <directory>


"""
        )

    def __get_options(self):
        options_dict = {
                        'size'      : [1, 'r'],
                        'format'    : [1, 'f'],
                        'help'      : [0],
                        'headings-to-sections'  : [0],
                        'group-styles'          : [0],
                        'no-lists'              : [0],
                        'level'         : [1],
                    }
                
        options_obj = txt2xml.options_trem.ParseOptions(
                system_string = sys.argv,
                options_dict = options_dict
            )

        options, arguments = options_obj.parse_options()
        if options == 0:
            sys.stderr.write('Script will now quit.\n')
            sys.exit(1)
        
        the_keys = options.keys()
        if 'help' in the_keys:
            self.__print_help_message()
            sys.exit(0)
        if len(arguments) != 1:
            sys.stderr.write('You must have one and only one directory as your argument.\n')
            sys.exit(1)
        the_dir = arguments[0]
        if not os.path.isdir(the_dir):
            sys.stderr.write('"%s" does not appear to be a directory.\n' % the_dir)
            sys.exit(1)
        self.__main_dir = arguments[0]
        if 'size' in the_keys:
            self.__size = int(options['size'])
        else:
            self.__size = None
        if 'format' in the_keys:
            format = options['format']
            if format != 'raw' and format != 'sdoc' and format != 'tei':
                sys.stderr.write('Format must be raw, sdoc, or tei\n'
                    )
                sys.exit(1)
            else:
                self.__format = format
        else:
            self.__format = None
        if 'headings-to-sections' in the_keys:
            self.__convert_headings = 1
        else:
            self.__convert_headings = 0

        if 'group-styles' in the_keys:
            self.__group_styles = 1
        else:
           self.__group_styles = 0

        if 'no-lists' in the_keys:
            self.__no_lists = 1
        else:
            self.__no_lists = 0
        self.__level = None
        if 'level' in  the_keys:
            self.__level =  options['level']

        arguments_dict = {}
        return arguments_dict


    def test_files(self):
        self.__get_options()
        self.__make_file_list(self.__main_dir)
        self.__sort_list()
        self.__print_first_tally()
        self.__execute_command()

if __name__ == '__main__':
    the_test_files_obj = TestFiles()
    the_test_files_obj.test_files()
