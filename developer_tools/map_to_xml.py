#!/usr/bin/env python

import os, re, sys
read_obj = open('/etc/rtf2xml_data/char_set', 'r')
line_to_read = 1
good = ['']
bad = ['unused', 'single_set', 'bottom_128', 'ms_symbol', 'ms_wingdings', 'ms_dingbats',
        'bottom_128_old','not_unicode', 'caps_hex','caps_letters','SYMBOL', 'ascii_to_hex',
        'wingdings', 'dingbats', 'caps_uni', 'caps_to_lower','wingdings_old',
        'SYMBOL_decimal', 'SYMBOL_old','greek', 'dingbats_old',
        'ms_set_old',
]
state = 'default'
ignore_names = [
    'MY UNDEFINED SYMBOL',
    'NUL',
    'LEFT SINGLE QUOTATION MARK',
    'RIGHT SINGLE QUOTATION MARK',
    'LEFT DOUBLE QUOTATION MARK',
    'RIGHT DOUBLE QUOTATION MARK',
   ]
sys.stdout.write('<ms-map>\n')
while line_to_read:
    line_to_read = read_obj.readline()
    print_lint = 1
    line = line_to_read
    line = line.strip()
    if line[0:9] == '<control>':
        continue
    if line[0:2] == '</':
        if state == 'default' or state == 'standard':
            sys.stdout.write('%s\n' % line)
        continue
    elif line[0:1] == '<':
        tag = line[1:-1]
        if tag in bad:
            state = 'ignore'
        elif tag == 'ms_standard':
            state = 'standard'
            sys.stdout.write('<%s>\n' % tag)
        else:
            state = 'default'
            sys.stdout.write('<%s>\n' % tag)
        continue
    elif line[0:1] == '#':
        continue
    if state == 'default':
        try:
            fields = line.split(':')
            name = fields[0]
            if name in ignore_names: 
                continue

            ms = fields[1][1:]
            dec = fields[2]
            hex = fields[3][3:-1]
            sys.stdout.write('    <character name="%s" unicode-prefix = "\\u" ms="%s" dec="%s" hex="%s" hex-prefix="\\\'" hex-suffix=" " value="&#x%s;" unicode-dec="%s"/>\n' %
                    (name, ms, dec, hex, hex, dec)
                    )

        except IndexError:
            sys.stderr.write(line)


    if state == 'standard':
        try:
            fields = line.split(':')
            name = fields[0]
            if name == 'NUL':
                continue
            ms = fields[1]
            dec = fields[2]
            hex = fields[3][3:-1]
            sys.stdout.write('    <character name="%s" unicode-prefix = "" ms="%s" dec="%s" hex="%s" hex-prefix="\\" hex-suffix=" " value = "&#x%s;"/>\n' %
                    (name, ms, dec, hex, hex)
                    )
        except IndexError:
            sys.stderr.write(line)

sys.stdout.write('</ms-map>\n')
