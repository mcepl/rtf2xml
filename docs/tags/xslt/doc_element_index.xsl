<xsl:stylesheet 
  xmlns:xsl = "http://www.w3.org/1999/XSL/Transform"
  version = "1.0"
>
    <xsl:template name = "make-index">
       <xsl:document href = "/home/paul/cvs/rtf2xml/docs/html/elements/elements-index.html">
        <html>
            <head>
                <title>
                    <xsl:value-of select="/article/articleinfo/title"/>
                </title>
            </head>
            <body>
                <xsl:apply-templates select = "/article/section[@role='elements']" mode = "index"/>
            </body>
        </html>
       </xsl:document>
    </xsl:template>

    <xsl:template match = "/article/section[@role='elements']" mode = "index">
        <h1 class = "page-title">Elements</h1>
        <xsl:apply-templates select = "section[@role='element']" mode = "index">
            <xsl:sort select = "title"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match = "/article/section[@role='elements']/section[@role='element']" mode = "index">
        <p class = "index-element">
            <xsl:element name = "a">
                <xsl:attribute name = "href">
                    <xsl:value-of select = "title"/>
                    <xsl:text>.html</xsl:text>
                </xsl:attribute>
                <xsl:value-of select = "title"/>
            </xsl:element>
        </p>
    </xsl:template>

</xsl:stylesheet>
