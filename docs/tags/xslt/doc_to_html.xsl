<xsl:stylesheet 
  xmlns:xsl = "http://www.w3.org/1999/XSL/Transform"
  version = "1.0"
>
    
    <xsl:include href = "/home/paul/cvs/rtf2xml/docs/tags/xslt/doc_element_index.xsl"/>
    <xsl:include href = "/home/paul/cvs/rtf2xml/docs/tags/xslt/doc_each_element.xsl"/>
    


    <xsl:template match="/">
        <xsl:call-template name = "make-index"/>
        <xsl:call-template name = "make-each-element"/>
    </xsl:template>

</xsl:stylesheet>
