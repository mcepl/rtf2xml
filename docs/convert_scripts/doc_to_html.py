#!/usr/bin/env python

import os, sys


xslt_dir = '/home/paul/cvs/rtf2xml/docs/xslt'
doc_dir = '/home/paul/cvs/rtf2xml/docs/xml'
html_dir = '/home/paul/cvs/rtf2xml/docs/html'

print 'putting together separte docs...'
command = 'xsltproc %s/put_together_for_html.xsl %s/master.xml \
     > arranged_temp.xml' % (xslt_dir, doc_dir)
os.system(command)


print 'rearranging doc...'
command = 'xsltproc %s/rearrange_html.xsl arranged_temp.xml \
     > rearranged_temp.xml' % (xslt_dir)
os.system(command)


print 'manpage parse...'
command = 'xsltproc %s/man_for_html.xsl rearranged_temp.xml \
     > man_temp.xml' % (xslt_dir)
os.system(command)


print 'converting rearrange.xml to numbers...'
command = 'xsltproc %s/number.xsl  man_temp.xml > numbered_temp.xml' % (xslt_dir)
os.system(command)


print 'converting numbered to toc.xml...'
command = 'xsltproc %s/toc.xsl numbered_temp.xml >toc_temp.xml' % (xslt_dir)
os.system(command)


print 'converting toc to html...'
command = 'xsltproc %s/docbook_to_html.xsl toc_temp.xml > %s/index.html' % (xslt_dir, html_dir)
os.system(command)

response = raw_input('do you want to upload docs? (Y, N)')
if response != 'Y':
    sys.exit(0)

os.chdir('/home/paul/cvs/rtf2xml/docs/html/main_page')
command = 'scp *.html *.css paultremblay@shell.sourceforge.net:/home/groups/r/rt/rtf2xml/htdocs'
print command
os.system(command)

os.chdir('/home/paul/cvs/rtf2xml/docs/html')
command = 'scp python_example.py *.html *.css paultremblay@shell.sourceforge.net:/home/groups/r/rt/rtf2xml/htdocs/docs'
print command
os.system(command)

"""

print 'assembling doc...'
command = 'xsltproc xslt/input.xsl master.xml > input.xml'
os.system(command)


print 'rearranging nodes in docbook...'
command = 'xsltproc xslt/rearrange_man.xsl input.xml > rearrange.xml'
os.system(command)



"""

