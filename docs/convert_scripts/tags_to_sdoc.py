#!/usr/bin/env python
import os, sys, tempfile, re
import options_trem

class Convert:

    def __init__( self, out_dir='/home/paul/cvs/rtf2xml/docs/tags/xml/'):
        self.__validation_file = '/home/paul/Documents/data/relax/elements.rxg'
        self.__out_dir = out_dir

    def convert_files(self, list_of_files):
        self.__prepare_temp_dir()
        self.__convert_all_files(list_of_files)


    def __prepare_temp_dir(self):
        if os.path.isdir('elements_temp_dir'):
            self.__empty_dir()
        os.mkdir('elements_temp_dir')
    
    def __empty_dir(self):
        command = 'rm -Rf elements_temp_dir'
        os.system(command)
    
    def __convert_all_files(self, list_of_files):
        list_of_valid_files = self.__make_list_of_files(list_of_files)
        for the_file in list_of_valid_files:
            out_file = self.__convert_str_to_xml(the_file)
            base_name, file_name = os.path.split(the_file)
            file_name, ext = os.path.splitext(file_name)
            self.__convert_xml_to_element(out_file, file_name)

        list_of_element_files = self.__list_of_element_files('elements_temp_dir')
        self.__validate_with_relax(list_of_element_files)
        self. __convert_to_sdoc(list_of_element_files)

    def __make_list_of_files(self, list_of_files):
        valid_files = []
        for the_file in list_of_files:
            is_valid = self.__file_is_valid(the_file)
            if is_valid:
                valid_files.append(the_file)

        return valid_files

    def __convert_str_to_xml(self, the_file):
        command = 'txt2doc %s > TEMP_XML.xml' % the_file
        os.system(command)
        return 'TEMP_XML.xml'

    def __convert_xml_to_element(self, the_file, original_file):
        out_file = '%s_element.xml' % original_file
        out_file = os.path.join('elements_temp_dir', out_file)
        command = 'xsltproc /home/paul/Documents/data/xsl_style_sheets/tags/to_tag.xsl\
          %s > %s' % (the_file, out_file)
        os.system(command)

    def __list_of_element_files(self, the_dir):
        list_of_files = []
        all_files = os.listdir(the_dir)
        for the_file in all_files:
            path = os.path.join(the_dir, the_file)
            list_of_files.append(path)
        return list_of_files

    def __validate_with_relax(self, list_of_files):
        file_string = ' '.join(list_of_files)
        file_string = ' ' + file_string
        command = 'validate_msv %s %s > validation_temp' % (
            self.__validation_file, file_string)
        os.system(command)


    def __file_is_valid(self, the_file):
        is_file = os.path.isfile(the_file)
        if not is_file:
            return 0
        file_name, ext = os.path.splitext(the_file)
        if ext == '.str':
            return 1
        return 0

    def __convert_to_sdoc(self, list_of_files):
        xsl_stylesheet = '/home/paul/Documents/data/xsl_style_sheets/tags/to_docbook_frag.xsl'
        for the_file in list_of_files:
            base_name, file_name = os.path.split(the_file)
            file_name, ext = os.path.splitext(file_name)
            file_name = file_name.replace('_element', '')
            file_name += '.xml'
            out_file = os.path.join(self.__out_dir, file_name)
            command = 'xsltproc %s %s > %s ' % (xsl_stylesheet, the_file, out_file)  
            os.system(command)



def print_help():
    print"""

tags_to_sdoc --help

tags_to_sdoc <list-of-files>

"""

def Main():

    options_dict = {
        'help':   [0, 'h'],
    }
                
    options_obj = options_trem.ParseOptions(
                system_string = sys.argv,
                options_dict = options_dict
    )
    options, arguments = options_obj.parse_options()
    list_of_options = options.keys()
    if 'help' in list_of_options:
        print_help()
        sys.exit(0)

    if len(arguments) < 1:
        sys.stderr.write('Please provide at least one file\n')
        print_help()
        sys.exit(1)

    convert_obj = Convert()

    convert_obj.convert_files(arguments)





if __name__ == '__main__':
    Main()
