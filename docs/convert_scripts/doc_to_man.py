#!/usr/bin/env python

import os, sys

xslt_dir = '/home/paul/cvs/rtf2xml/docs/xslt'
doc_dir = '/home/paul/cvs/rtf2xml/docs/xml'

# print 'putting together separte docs...'
# command = 'xsltproc %s/put_together_for_man.xsl %s/master.xml \
    # > arranged_temp.xml' % (xslt_dir, doc_dir)
# os.system(command)


print 'converting man.xml to block.xml...'
command = 'xsltproc %s/docbook_to_man.xsl %s/man_page.xml > block.xml' % (xslt_dir, doc_dir)
os.system(command)

print 'converting with script to ?.txt...'
command = 'xml2text.py --output rtf2xml.1 block.xml'
os.system(command)

os.system('cp rtf2xml.1 rtf2xml_copy.1')


# print 'covnert to bunzip..'
# command = 'bzip2 trem_backup.1'
# os.system(command)

user_response = raw_input('Do you want to copy this to "/usr/share/man/man1"?\n')
if user_response == 'y':
    print 'copy it to local man \n'
    command = 'cp rtf2xml.1 /usr/share/man/man1'
    os.system(command)


print 'copy it to data_files \n'
command = 'cp rtf2xml.1 ../../data'
os.system(command)

print 'copy it to man page directory \n'
command = 'cp rtf2xml.1 ../man'
os.system(command)

# print 'remove old bunzip file..'
# os.remove('trem_backup.1.bz2')
