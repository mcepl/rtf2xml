<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY copyright "Copyright &#x00A9; 2004">
    <!ENTITY namespace "http://rtf2xml.sourceforge.net/">
    <!ENTITY current-dtd "http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd">
    <!ENTITY tei-dtd "http://rtf2xml.sourceforge.net/dtd/teilite.dtd">
]>
<!--
#########################################################################
#                                                                       #
#                                                                       #
#   copyright 2002 Paul Henry Tremblay                                  #
#                                                                       #
#   This program is distributed in the hope that it will be useful,     #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of      #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU    #
#   General Public License for more details.                            #
#                                                                       #
#   You should have received a copy of the GNU General Public License   #
#   along with this program; if not, write to the Free Software         #
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA            #
#   02111-1307 USA                                                      #
#                                                                       #
#                                                                       #
#########################################################################
-->
<xsl:stylesheet 
    version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:doc="http://www.kielen.com/documentation"
    exclude-result-prefixes="rtf doc"
    xmlns:rtf="http://rtf2xml.sourceforge.net/"
    >


 <doc:stylesheet >
   <doc:name>header.xsl</doc:name>
   <doc:used-by>
     <doc:stylesheet-name>header.xsl</doc:stylesheet-name>
     </doc:used-by>
   <doc:uses/>
   <doc:parameters/>
   <doc:entities>
     <doc:entity name="copyright">Copyright &#x00A9; 2004</doc:entity>
     <doc:entity name="namespace">http://rtf2xml.sourceforge.net/</doc:entity>
     <doc:entity name="current-dtd">http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd</doc:entity>
     <doc:entity name="tei-dtd">http://rtf2xml.sourceforge.net/dtd/teilite.dtd</doc:entity>
   </doc:entities>
   <doc:description>
     <doc:para>&copyright; by Paul Tremblay</doc:para>
     <doc:para>The namespace "rtf" is mapped to "&namespace;".</doc:para>
     <doc:para>This stylesheet converts various time elements.</doc:para>
   </doc:description>
 </doc:stylesheet>

 <doc:template  
 xmlns:doc="http://www.kielen.com/documentation">
   <doc:name>month-name</doc:name>
   <doc:type>name</doc:type>
   <doc:parameters/>
   <doc:called-by>
     <doc:template-name>header.xsl#?</doc:template-name>
   </doc:called-by>
   <doc:description>
     <doc:para>Converts a number to a month name.</doc:para>
   </doc:description>
 </doc:template>

    <xsl:template name = "month-name">
        <xsl:param name = "num"></xsl:param>
        <xsl:choose>
            <xsl:when test = "$num='1'">
                <xsl:text>January</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='2'">
                <xsl:text>February</xsl:text>
            </xsl:when>
            <xsl:when test = "$num = '3'">
                <xsl:text>March</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='4'">
                <xsl:text>April</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='5'">
                <xsl:text>May</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='6'">
                <xsl:text>June</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='7'">
                <xsl:text>July</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='8'">
                <xsl:text>August</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='9'">
                <xsl:text>September</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='10'">
                <xsl:text>October</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='11'">
                <xsl:text>November</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='12'">
                <xsl:text>December</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>


 <doc:template  
 xmlns:doc="http://www.kielen.com/documentation">
   <doc:name>month-number</doc:name>
   <doc:type>name</doc:type>
   <doc:parameters/>
   <doc:called-by>
     <doc:template-name>header.xsl#?</doc:template-name>
   </doc:called-by>
   <doc:description>
     <doc:para>Converts a number to a two-decimal month number.</doc:para>
   </doc:description>
 </doc:template>

    <xsl:template name = "month-number">
        <xsl:param name = "num"></xsl:param>
        <xsl:choose>
            <xsl:when test = "$num='1'">
                <xsl:text>01</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='2'">
                <xsl:text>02</xsl:text>
            </xsl:when>
            <xsl:when test = "$num = '3'">
                <xsl:text>03</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='4'">
                <xsl:text>04</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='5'">
                <xsl:text>05</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='6'">
                <xsl:text>06</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='7'">
                <xsl:text>07</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='8'">
                <xsl:text>08</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='9'">
                <xsl:text>09</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='10'">
                <xsl:text>10</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='11'">
                <xsl:text>11</xsl:text>
            </xsl:when>
            <xsl:when test = "$num='12'">
                <xsl:text>11</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
