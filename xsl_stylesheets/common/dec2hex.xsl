<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    version="1.0" 
    >
    <xsl:template name="dec-to-hex">
        <xsl:param name="value">0</xsl:param>
        <xsl:param name="start-value">1</xsl:param>
        <xsl:variable name="test-digit">
            <xsl:value-of select="$value - ($start-value * 16)"/>
        </xsl:variable>
        <xsl:variable name = "test-is-number">
            <xsl:value-of select = "$value + 1"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test = "$test-is-number = 'NaN'">
               <xsl:text>NaN</xsl:text> 
            </xsl:when>
            <xsl:when test = "$value &gt; 255">
                <xsl:text>NaN</xsl:text>
            </xsl:when>
            <xsl:when test="$test-digit &lt; 0">
                <xsl:variable name="second-place" select="$start-value - 1"/>
                <xsl:call-template name="single-dec-to-hex">
                    <xsl:with-param name="value" select="$second-place"/>
                </xsl:call-template>
                <xsl:variable name="first-place">
                    <xsl:value-of select="$value - $second-place * 16"/>
                </xsl:variable>
                <xsl:call-template name="single-dec-to-hex">
                    <xsl:with-param name="value" select="$first-place"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="new-start-value" select="$start-value + 1"/>
                <xsl:call-template name="dec-to-hex">
                    <xsl:with-param name="value" select="$value"/>
                    <xsl:with-param name="start-value" select="$new-start-value"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>



<!--
Template used to convert two-digit hexidecimals to base-10 numbers.
First get the ones digit and use the hex-to-dec to convert this to 
a base-10 number.

Get the first digit and use the hex-to-dec to convert this to 
a base-10 number. Multiply this number by 16. 

Add the ones and the tens variable to get the number

-->
    <xsl:template name="convert-hex">
        <xsl:param name="num">00</xsl:param>
        <xsl:variable name="ones">
            <xsl:call-template name="hex-to-dec">
                <xsl:with-param name="digit" select="substring($num, 2, 1)"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="tens">
            <xsl:variable name="tens-digit">
                <xsl:call-template name="hex-to-dec">
                    <xsl:with-param name="digit" select="substring($num, 1, 1)"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:value-of select="$tens-digit * 16"/>
        </xsl:variable>
        <xsl:value-of select="$ones + $tens"/>
    </xsl:template>
    

    <xsl:template name="single-dec-to-hex">
        <xsl:param name="value">0</xsl:param>
        <xsl:choose>
            <xsl:when test="$value = '0'">
                <xsl:text>0</xsl:text>
            </xsl:when>
            <xsl:when test="$value = '1'">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:when test="$value = '2'">
                <xsl:text>2</xsl:text>
            </xsl:when>
            <xsl:when test="$value = '3'">
                <xsl:text>3</xsl:text>
            </xsl:when>
            <xsl:when test="$value = '4'">
                <xsl:text>4</xsl:text>
            </xsl:when>
            <xsl:when test="$value = '5'">
                <xsl:text>5</xsl:text>
            </xsl:when>
            <xsl:when test="$value = '6'">
                <xsl:text>6</xsl:text>
            </xsl:when>
            <xsl:when test="$value = '7'">
                <xsl:text>7</xsl:text>
            </xsl:when>
            <xsl:when test="$value = '8'">
                <xsl:text>8</xsl:text>
            </xsl:when>
            <xsl:when test="$value = '9'">
                <xsl:text>9</xsl:text>
            </xsl:when>
            <xsl:when test="$value = '10'">
                <xsl:text>a</xsl:text>
            </xsl:when>
            <xsl:when test="$value = '11'">
                <xsl:text>b</xsl:text>
            </xsl:when>
            <xsl:when test="$value = '12'">
                <xsl:text>c</xsl:text>
            </xsl:when>
            <xsl:when test="$value = '13'">
                <xsl:text>d</xsl:text>
            </xsl:when>
            <xsl:when test="$value = '14'">
                <xsl:text>e</xsl:text>
            </xsl:when>
            <xsl:when test="$value = '15'">
                <xsl:text>f</xsl:text>
            </xsl:when>
        </xsl:choose>

    </xsl:template>


<!--

Template converts one hexidecimal to a base-10 number.
-->


    <xsl:template name="hex-to-dec">
        <xsl:param name="digit">0</xsl:param>
        <xsl:choose>
            <xsl:when test="$digit = '0'">
                <xsl:text>0</xsl:text>
            </xsl:when>
            <xsl:when test="$digit = '1'">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:when test="$digit = '2'">
                <xsl:text>2</xsl:text>
            </xsl:when>
            <xsl:when test="$digit = '3'">
                <xsl:text>3</xsl:text>
            </xsl:when>
            <xsl:when test="$digit = '4'">
                <xsl:text>4</xsl:text>
            </xsl:when>
            <xsl:when test="$digit = '5'">
                <xsl:text>5</xsl:text>
            </xsl:when>
            <xsl:when test="$digit = '6'">
                <xsl:text>6</xsl:text>
            </xsl:when>
            <xsl:when test="$digit = '7'">
                <xsl:text>7</xsl:text>
            </xsl:when>
            <xsl:when test="$digit = '8'">
                <xsl:text>8</xsl:text>
            </xsl:when>
            <xsl:when test="$digit = '9'">
                <xsl:text>9</xsl:text>
            </xsl:when>
            <xsl:when test="$digit = 'A' or $digit = 'a'">
                <xsl:text>10</xsl:text>
            </xsl:when>
            <xsl:when test="$digit = 'B' or $digit = 'b'">
                <xsl:text>11</xsl:text>
            </xsl:when>
            <xsl:when test="$digit = 'C' or $digit = 'c'">
                <xsl:text>12</xsl:text>
            </xsl:when>
            <xsl:when test="$digit = 'D' or $digit = 'd'">
                <xsl:text>13</xsl:text>
            </xsl:when>
            <xsl:when test="$digit = 'E' or $digit = 'e'">
                <xsl:text>14</xsl:text>
            </xsl:when>
            <xsl:when test="$digit = 'F' or $digit = 'f'">
                <xsl:text>15</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
