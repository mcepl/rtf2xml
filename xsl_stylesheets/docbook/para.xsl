<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY copyright "Copyright &#x00A9; 2004">
    <!ENTITY namespace "http://rtf2xml.sourceforge.net/">
    <!ENTITY current-dtd "http://rtf2xml.sourceforge.net/dtd/rtf2xml.6.dtd">
]><!--
#########################################################################
#                                                                       #
#                                                                       #
#   copyright 2002 Paul Henry Tremblay                                  #
#                                                                       #
#   This program is distributed in the hope that it will be useful,     #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of      #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU    #
#   General Public License for more details.                            #
#                                                                       #
#   You should have received a copy of the GNU General Public License   #
#   along with this program; if not, write to the Free Software         #
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA            #
#   02111-1307 USA                                                      #
#                                                                       #
#                                                                       #
#########################################################################
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:doc="http://www.kielen.com/documentation" exclude-result-prefixes="rtf doc" xmlns:rtf="http://rtf2xml.sourceforge.net/">


    <doc:stylesheet>
        <doc:name>para.xsl</doc:name>
        <doc:used-by>
            <doc:stylesheet-name>to_sdoc.xsl</doc:stylesheet-name>
        </doc:used-by>
        <doc:uses/>
        <doc:parameters/>
        <doc:entities>
            <doc:entity name="copyright">Copyright &#x00A9; 2004</doc:entity>
            <doc:entity name="namespace">http://rtf2xml.sourceforge.net/</doc:entity>
            <doc:entity name="current-dtd">http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd</doc:entity>
        </doc:entities>
        <doc:description>
            <doc:para>&copyright; by Paul Tremblay</doc:para>
            <doc:para>The namespace "rtf" is mapped to "&namespace;".</doc:para>
            <doc:para>This stylesheet handles para elements.</doc:para>
        </doc:description>
    </doc:stylesheet>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:para</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:calls>
            <doc:template-name>#para</doc:template-name>
        </doc:calls>
        <doc:called-by/>
        <doc:description>
            <doc:para>Check if the para element is really a title for a section. If so, do not wrap the para 
            element with the &lt;p&gt; element.</doc:para>
            <doc:para>Otherwise create a &lt;p&gt; element by calling the para template.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:para">
        <xsl:choose>
            <xsl:when test = "parent::rtf:paragraph-definition[@name='heading 1']|
                              parent::rtf:paragraph-definition[@name='heading 2']|
                              parent::rtf:paragraph-definition[@name='heading 3']|
                              parent::rtf:paragraph-definition[@name='heading 4']|
                              parent::rtf:paragraph-definition[@name='heading 5']|
                              parent::rtf:paragraph-definition[@name='heading 6']|
                              parent::rtf:paragraph-definition[@name='heading 7']|
                              parent::rtf:paragraph-definition[@name='heading 8']|
                              parent::rtf:paragraph-definition[@name='heading 9']
            
            ">
                <xsl:variable name = "heading-name" select = "../@name"/>
                <xsl:choose>
                    <xsl:when test = "name(ancestor::*[3]) = 'section' and ../../../@type=$heading-name">
                        <xsl:apply-templates/>
                    </xsl:when>
                    <xsl:when test = "name(ancestor::*[2]) = 'border-group' and 
                        name(ancestor::*[4]) = 'section'  and ../../../../@type=$heading-name">
                        <xsl:apply-templates/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name = "para"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name = "para"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>



    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>para</doc:name>
        <doc:type>name</doc:type>
        <doc:parameters/>
        <doc:called-by>
            <doc:template-name>#rtf:para</doc:template-name>
        </doc:called-by>
        <doc:description>
            <doc:para>Create a role element based on the name of the parent paragraph-definition.</doc:para>
            <doc:para>Apply templates within this element.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template name = "para">
        <xsl:if test = "normalize-space(.) or child::*">
            <xsl:choose>
                <xsl:when test = "ancestor::rtf:cell">
                    <xsl:call-template name = "para-content"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:element name = "para">
                        <xsl:attribute name = "role">
                            <xsl:value-of select = "../@name"/>
                        </xsl:attribute>
                        <xsl:call-template name = "para-content"/>
                    </xsl:element>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>



    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>para-content</doc:name>
        <doc:type>name</doc:type>
        <doc:parameters/>
        <doc:called-by>
            <doc:template-name>#para</doc:template-name>
        </doc:called-by>
        <doc:description>
            <doc:para>Create appropriate role attribute.</doc:para>
            <doc:para>Apply templates within this element.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template name = "para-content">
        <xsl:choose>
            <xsl:when test = "../@italics = 'true' ">
               <emphasis role = "paragraph-emph-italics">
                    <xsl:apply-templates/>
               </emphasis> 
            </xsl:when>
            <xsl:when test = "../@bold = 'true' ">
               <emphasis role = "paragraph-emph-bold">
                    <xsl:apply-templates/>
               </emphasis> 
            </xsl:when>
            <xsl:when test = "../@underlined">
               <emphasis role = "paragraph-emph-underlined">
                    <xsl:apply-templates/>
               </emphasis> 
            </xsl:when>
            <xsl:when test = "(../@strike-through = 'true')
                or (../@double-strike-through = 'true')
                or (../@emboss = 'true')
                or (../@engrave = 'true')
                or (../@small-caps = 'true')
                or (../@shadow = 'true')
                or (../@hidden = 'true')
                or (../@outline = 'true')
            
                ">
               <emphasis role = "paragraph-emph">
                    <xsl:apply-templates/>
               </emphasis> 
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
