<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY copyright "Copyright &#x00A9; 2004">
    <!ENTITY namespace "http://rtf2xml.sourceforge.net/">
    <!ENTITY current-dtd "http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd">
]><!--
#########################################################################
#                                                                       #
#                                                                       #
#   copyright 2002 Paul Henry Tremblay                                  #
#                                                                       #
#   This program is distributed in the hope that it will be useful,     #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of      #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU    #
#   General Public License for more details.                            #
#                                                                       #
#   You should have received a copy of the GNU General Public License   #
#   along with this program; if not, write to the Free Software         #
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA            #
#   02111-1307 USA                                                      #
#                                                                       #
#                                                                       #
#########################################################################
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:doc="http://www.kielen.com/documentation" exclude-result-prefixes="rtf doc" xmlns:rtf="http://rtf2xml.sourceforge.net/">


    <doc:stylesheet>
        <doc:name>lists.xsl</doc:name>
        <doc:used-by>
            <doc:stylesheet-name>to_sdoc.xsl</doc:stylesheet-name>
        </doc:used-by>
        <doc:uses/>
        <doc:parameters/>
        <doc:entities>
            <doc:entity name="copyright">Copyright &#x00A9; 2004</doc:entity>
            <doc:entity name="namespace">http://rtf2xml.sourceforge.net/</doc:entity>
            <doc:entity name="current-dtd">http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd</doc:entity>
        </doc:entities>
        <doc:description>
            <doc:para>&copyright; by Paul Tremblay</doc:para>
            <doc:para>The namespace "rtf" is mapped to "&namespace;".</doc:para>
            <doc:para>This stylesheet handles all elements in the list element.</doc:para>
        </doc:description>
    </doc:stylesheet>



    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>list-text</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>delete the element &lt;list-text&gt; if the variable "delte-list-text is true.</doc:para>
            <doc:para>Otherwise apply templates without creating a new element.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:list-text">
        <xsl:choose>
            <xsl:when test = "$delete-list-text='true'"/>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>list(ordered)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Make a &lt;list&gt; element and apply child elements.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:list[@list-type='ordered']">
        <xsl:element name = "orderedlist">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>list(unordered)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Make a &lt;list&gt; element and apply child elements.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:list[@list-type='unordered']">
        <xsl:element name = "itemizedlist">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>item</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Make a &lt;item&gt; element and apply child elements.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:item">
        <xsl:element name = "listitem">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>


</xsl:stylesheet>
