<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY copyright "Copyright &#x00A9; 2004">
    <!ENTITY namespace "http://rtf2xml.sourceforge.net/">
    <!ENTITY current-dtd "http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd">
]><!--
#########################################################################
#                                                                       #
#                                                                       #
#   copyright 2002 Paul Henry Tremblay                                  #
#                                                                       #
#   This program is distributed in the hope that it will be useful,     #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of      #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU    #
#   General Public License for more details.                            #
#                                                                       #
#   You should have received a copy of the GNU General Public License   #
#   along with this program; if not, write to the Free Software         #
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA            #
#   02111-1307 USA                                                      #
#                                                                       #
#                                                                       #
#########################################################################
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:doc="http://www.kielen.com/documentation" exclude-result-prefixes="rtf doc" xmlns:rtf="http://rtf2xml.sourceforge.net/">


    <doc:stylesheet>
        <doc:name>blocks.xsl</doc:name>
        <doc:used-by>
            <doc:stylesheet-name>to_sdoc.xsl</doc:stylesheet-name>
        </doc:used-by>
        <doc:uses/>
        <doc:parameters/>
        <doc:entities>
            <doc:entity name="copyright">Copyright &#x00A9; 2004</doc:entity>
            <doc:entity name="namespace">http://rtf2xml.sourceforge.net/</doc:entity>
            <doc:entity name="current-dtd">http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd</doc:entity>
        </doc:entities>
        <doc:description>
            <doc:para>&copyright; by Paul Tremblay</doc:para>
            <doc:para>The namespace "rtf" is mapped to "&namespace;".</doc:para>
            <doc:para>This stylesheet handles block elements such as body and section.</doc:para>
        </doc:description>
    </doc:stylesheet>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:header-or-footer</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:description>
            <doc:para>Deletes rtf:header-of-footer element.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:header-or-footer"/>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:body</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:description>
            <doc:para>Matches body element and applies all other templates.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:body">
        <xsl:apply-templates/>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:section(native)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:description>
            <doc:para>Matches section element and applies all other templates.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:section[@type='rtf-native']">
        <xsl:variable name="is-field-block">
            <xsl:call-template name = "check-section"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test = "not(normalize-space(.))"/>
            <xsl:when test = "$is-field-block='true'">
                <xsl:choose>
                    <xsl:when test="$delete-field-blocks='true'"/>
                    <xsl:otherwise>
                        <xsl:call-template name = "section-native">
                            <xsl:with-param name = "type" select = "'header'"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name = "section-native">
                    <xsl:with-param name = "type">
                        <xsl:text>native</xsl:text>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>check-section</doc:name>
        <doc:type>name</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:description>
            <doc:para>Check if the section contains just a field block.</doc:para>
            <doc:para>Returns 'true' if the section is only for a field-block.</doc:para>
            <doc:para>Returns 'false' if the section contains other elements.</doc:para>
            <doc:para>If the templates are going to delete field blocks, we need 
            to delete the sections that wrap them, but only if the section is meant just
            for field blocks, as is usually the case with an index.</doc:para>
            <doc:para>Check if the section contains a field-block. If so, check that it contains
            only field-blocks of the same type. If it contains other children, or if it contains
            normal paragraphs with text, the section is not meant for just field-blocks.</doc:para>
            <doc:para>If the section contains only children with of the same field-block type
            as the first, the section is meant for just the field block.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template name = "check-section">
        <xsl:choose>
            <xsl:when test = "rtf:field-block[@type='index']|
                              rtf:field-block[@type='table-of-contents']|
                              rtf:field-block[@type='table-of-authorities']
                              "
            >

            <xsl:variable name = "type" select = "rtf:field-block/@type"/>
                <xsl:choose>
                <!--
                The first when should cover all cases. The others are there because I'm 
                note sure.
                -->
                    <xsl:when test = "child::*[not(self::rtf:field-block[@type=$type])]">
                        <xsl:text>false</xsl:text>
                    </xsl:when>
                    <xsl:when test = "rtf:style-group/rtf:paragraph-definition/rtf:para/text()">
                        <xsl:text>false</xsl:text>
                    </xsl:when>
                    <xsl:when test = "normalize-space(child::*[not(self::rtf:field-block[@type=$type])])">
                        <xsl:text>false</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>true</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>false</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>section(native)</doc:name>
        <doc:type>name</doc:type>
       <doc:parameters>
            <doc:parameter name="type">The type of div, such as div0, index, toc, or toa.</doc:parameter>
       </doc:parameters>
        <doc:called-by>
            <doc:template-name>#rtf:section(native)</doc:template-name>
        </doc:called-by>
        <doc:calls/>
        <doc:description>
            <doc:para>Form sections.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template name = "section-native">
        <xsl:param name = "type"/>
            <xsl:element name = "section">
                <xsl:attribute name = "role">
                    <xsl:value-of select = "$type"/>
                </xsl:attribute>
                <xsl:element name = "title"/>
                <xsl:apply-templates/>
            </xsl:element>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:section(for headers)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls>
            <doc:template-name>#section-header</doc:template-name>
        </doc:calls>
        <doc:description>
            <doc:para>Matches section element and applies all other templates.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:section[@type='heading 1']|
                           rtf:section[@type='heading 2']|
                           rtf:section[@type='heading 3']|
                           rtf:section[@type='heading 4']|
                           rtf:section[@type='heading 5']|
                           rtf:section[@type='heading 6']|
                           rtf:section[@type='heading 7']|
                           rtf:section[@type='heading 8']|
                           rtf:section[@type='heading 9']
    
    ">
        <xsl:variable name = "type" select = "@type"/>
        <xsl:element name = "section">
            <xsl:attribute name = "type">
                <xsl:text>header</xsl:text>
                <xsl:value-of select = "@level"/>
            </xsl:attribute>
            <xsl:if test = "child::rtf:style-group[1][@name=$type]">
                <xsl:for-each select = "child::rtf:style-group[1][@name=$type]">
                    <xsl:call-template name = "section-header"/>
                </xsl:for-each>
            </xsl:if>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>section-header</doc:name>
        <doc:type>name</doc:type>
        <doc:parameters/>
        <doc:called-by>
            <doc:template-name>#rtf:section(for headers)</doc:template-name>
        </doc:called-by>
        <doc:description>
            <doc:para>A section header has been found. 
            Wrap the contents in &lt;head&gt; element and apply all children elements.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template name = "section-header">
        <title>
            <xsl:apply-templates/>
        </title>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:stye-group(headings)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:description>
            <doc:para>This template matches the element named style-group with heading names. 
            If this style group has as its parent a section, then delete it.</doc:para>
            <doc:para>Otherwise, treat is as a paragraph and apply the normal templates</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:style-group[@name='heading 1']|
                           rtf:style-group[@name='heading 2']|
                           rtf:style-group[@name='heading 3']|
                           rtf:style-group[@name='heading 4']|
                           rtf:style-group[@name='heading 5']|
                           rtf:style-group[@name='heading 6']|
                           rtf:style-group[@name='heading 7']|
                           rtf:style-group[@name='heading 8']|
                           rtf:style-group[@name='heading 9']
    
    ">
        <xsl:choose>
            <xsl:when test = "parent::rtf:section[1]"/>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:paragraph-definition</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:description>
            <doc:para>This element serves as a wrapper element for all paragraphs with the same style name. 
            Don't use this wrapper. Instead, apply all children templates.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:paragraph-definition">
        <xsl:apply-templates/>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:field-blocks</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Don't create a &lt;section&gt; element for this section.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:field-block">
        <xsl:choose>
            <xsl:when test = "$delete-field-blocks = 'true'"/>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:footnote</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Create a &lt;note&gt; element.</doc:para>
            <doc:para>Create either a "footnote" or "endnote" attribute.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:footnote">
        <xsl:element name = "footnote">
            <xsl:choose>
                <xsl:when test = "@type='endnote'">
                    <xsl:attribute name = "role">
                        <xsl:text>endnote</xsl:text>
                    </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>


    </xsl:stylesheet>

