<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY copyright "Copyright &#x00A9; 2004">
    <!ENTITY namespace "http://rtf2xml.sourceforge.net/">
    <!ENTITY current-dtd "http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd">
]><!--
#########################################################################
#                                                                       #
#                                                                       #
#   copyright 2002 Paul Henry Tremblay                                  #
#                                                                       #
#   This program is distributed in the hope that it will be useful,     #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of      #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU    #
#   General Public License for more details.                            #
#                                                                       #
#   You should have received a copy of the GNU General Public License   #
#   along with this program; if not, write to the Free Software         #
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA            #
#   02111-1307 USA                                                      #
#                                                                       #
#                                                                       #
#########################################################################
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:doc="http://www.kielen.com/documentation" exclude-result-prefixes="rtf doc" xmlns:rtf="http://rtf2xml.sourceforge.net/">


    <doc:stylesheet>
        <doc:name>fields.xsl</doc:name>
        <doc:used-by>
            <doc:stylesheet-name>to_sdoc.xsl</doc:stylesheet-name>
        </doc:used-by>
        <doc:uses/>
        <doc:parameters/>
        <doc:entities>
            <doc:entity name="copyright">Copyright &#x00A9; 2004</doc:entity>
            <doc:entity name="namespace">http://rtf2xml.sourceforge.net/</doc:entity>
            <doc:entity name="current-dtd">http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd</doc:entity>
        </doc:entities>
        <doc:description>
            <doc:para>&copyright; by Paul Tremblay</doc:para>
            <doc:para>The namespace "rtf" is mapped to "&namespace;".</doc:para>
            <doc:para>This stylesheet handles field elements.</doc:para>
        </doc:description>
    </doc:stylesheet>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:field(index-entry)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handle field elements that have the type attribute of index-entry.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:field[@type='index-entry']">
        <xsl:element name="phrase">
            <xsl:attribute name="role">
                <xsl:text>index-entry:</xsl:text>
                <xsl:value-of select="@main-entry"/>
            </xsl:attribute>
        </xsl:element>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:field(include-pict)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handle field elements that have the type attribute of include-pict.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:field[@type='include-picture']">
        <mediaobject>
            <imageobject>
                <xsl:element name = "imagedata">
                    <xsl:attribute name = "fileref">
                        <xsl:value-of select = "@argument"/>
                    </xsl:attribute>
                </xsl:element>
            </imageobject>
        </mediaobject>
    </xsl:template>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:field(hyperlink)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handle field elements that have the type attribute of hyper-link.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:field[@type='hyperlink']">
        <xsl:element name ="ulink">
            <xsl:attribute name = "url">
               <xsl:value-of select = "@argument"/> 
            </xsl:attribute>
        </xsl:element>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:field(link)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handle field elements that have the type attribute of link.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:field[@type='link']">
        <xsl:element name ="ref">
            <xsl:attribute name = "url">
               <xsl:value-of select = "@argument"/> 
            </xsl:attribute>
        </xsl:element>
    </xsl:template>



    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:field(delete)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>delete he following fields.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:field[@type='editing-time']|
                           rtf:field[@type='number-of-characters-in-doc']|
                           rtf:field[@type='number-of-pages-in-doc']|
                           rtf:field[@type='number-of-words-in-doc']|
                           rtf:field[@type='number-of-pages-in-section']|
                           rtf:field[@type='file-name']|
                           rtf:field[@type='auto-num-out']|
                           rtf:field[@type='compare']|
                           rtf:field[@type='document-variable']|
                           rtf:field[@type='go-button']|
                           rtf:field[@type='next']|
                           rtf:field[@type='next-if']|
                           rtf:field[@type='skip-if']|
                           rtf:field[@type='if']|
                           rtf:field[@type='merge-field']|
                           rtf:field[@type='merge-record']|
                           rtf:field[@type='merge-sequence']|
                           rtf:field[@type='place-holder']|
                           rtf:field[@type='private']|
                           rtf:field[@type='referenced-document']|
                           rtf:field[@type='equation']|
                           rtf:field[@type='advance']|
                           rtf:field[@type='prompt-user']|
                           rtf:field[@type='barcode']|
                           rtf:field[@type='fill-in']|
                           rtf:field[@type='print']|
                           rtf:field[@type='form-checkbox']|
                           rtf:field[@type='form-text']|
                           rtf:field[@type='set']
    
    "/>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:field(default)</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Keep these feilds without doing anyting with them.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:field[@type='revision-number']|
                           rtf:field[@type='insert-section-number']|
                           rtf:field[@type='quote']|
                           rtf:field[@type='last-saved']|
                           rtf:field[@type='insert-time']|
                           rtf:field[@type='user-name']|
                           rtf:field[@type='key-words']|
                           rtf:field[@type='last-saved-by']|
                           rtf:field[@type='subject']|
                           rtf:field[@type='based-on-template']|
                           rtf:field[@type='document-title']|
                           rtf:field[@type='user-address']|
                           rtf:field[@type='user-name']|
                           rtf:field[@type='include-text-from-file']|
                           rtf:field[@type='reference-to-note']|
                           rtf:field[@type='reference-to-page']|
                           rtf:field[@type='reference']|
                           rtf:field[@type='numbering-sequence']|
                           rtf:field[@type='symbol']|
                           rtf:field[@type='anchor-for-table-of-authorities']|
                           rtf:field[@type='automatic-number']|
                           rtf:field[@type='auto-list-text']|
                           rtf:field[@type='auto-text']|
                           rtf:field[@type='contact']|
                           rtf:field[@type='database']|
                           rtf:field[@type='date']|
                           rtf:field[@type='document-property']|
                           rtf:field[@type='file-size']|
                           rtf:field[@type='document-info']|
                           rtf:field[@type='page']|
                           rtf:field[@type='style-reference']|
                           rtf:field[@type='user-property']
    
    ">
        <xsl:apply-templates/>
    </xsl:template>



</xsl:stylesheet>
