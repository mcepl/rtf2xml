<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY copyright "Copyright &#x00A9; 2004">
    <!ENTITY namespace "http://rtf2xml.sourceforge.net/">
    <!ENTITY current-dtd "http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd">
    <!ENTITY tei-dtd "http://rtf2xml.sourceforge.net/dtd/teilite.dtd">
]><!--
#########################################################################
#                                                                       #
#                                                                       #
#   copyright 2002 Paul Henry Tremblay                                  #
#                                                                       #
#   This program is distributed in the hope that it will be useful,     #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of      #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU    #
#   General Public License for more details.                            #
#                                                                       #
#   You should have received a copy of the GNU General Public License   #
#   along with this program; if not, write to the Free Software         #
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA            #
#   02111-1307 USA                                                      #
#                                                                       #
#                                                                       #
#########################################################################
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:doc="http://www.kielen.com/documentation" exclude-result-prefixes="rtf doc" xmlns:rtf="http://rtf2xml.sourceforge.net/">


    <doc:stylesheet>
        <doc:name>tables.xsl</doc:name>
        <doc:used-by>
            <doc:stylesheet-name>to_tei.xsl</doc:stylesheet-name>
        </doc:used-by>
        <doc:uses/>
        <doc:parameters/>
        <doc:entities>
            <doc:entity name="copyright">Copyright &#x00A9; 2004</doc:entity>
            <doc:entity name="namespace">http://rtf2xml.sourceforge.net/</doc:entity>
            <doc:entity name="current-dtd">http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd</doc:entity>
            <doc:entity name="tei-dtd">http://rtf2xml.sourceforge.net/dtd/teilite.dtd</doc:entity>
        </doc:entities>
        <doc:description>
            <doc:para>&copyright; by Paul Tremblay</doc:para>
            <doc:para>The namespace "rtf" is mapped to "&namespace;".</doc:para>
            <doc:para>This stylesheet handles all elements in the table element.</doc:para>
        </doc:description>
    </doc:stylesheet>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:table</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;table&gt;element.
            </doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:table">
        <xsl:element name = "table">
            <xsl:attribute name = "cols">
                <xsl:value-of select = "@number-of-columns"/>
            </xsl:attribute>
            <xsl:attribute name = "rows">
                <xsl:value-of select = "@number-of-rows"/>
            </xsl:attribute>
            <xsl:apply-templates/> 
        </xsl:element>
    </xsl:template>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:row</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;row&gt;element.
            </doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:row">
        <xsl:choose>
            <xsl:when test = "normalize-space(.)">
                <xsl:element name = "row">
                    <xsl:if test = "@header = 'true'">
                        <xsl:attribute name = "role">
                            <xsl:text>header</xsl:text>
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:cell</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls/>
        <doc:description>
            <doc:para>Handles inline elements within the &lt;cell&gt;element.
            </doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match = "rtf:cell">
        <xsl:element name = "cell">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>
