<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY copyright "Copyright &#x00A9; 2004">
    <!ENTITY namespace "http://rtf2xml.sourceforge.net/">
    <!ENTITY current-dtd "http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd">
    <!ENTITY tei-dtd "http://rtf2xml.sourceforge.net/dtd/teilite.dtd">
]><!--
#########################################################################
#                                                                       #
#                                                                       #
#   copyright 2002 Paul Henry Tremblay                                  #
#                                                                       #
#   This program is distributed in the hope that it will be useful,     #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of      #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU    #
#   General Public License for more details.                            #
#                                                                       #
#   You should have received a copy of the GNU General Public License   #
#   along with this program; if not, write to the Free Software         #
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA            #
#   02111-1307 USA                                                      #
#                                                                       #
#                                                                       #
#########################################################################
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:doc="http://www.kielen.com/documentation" exclude-result-prefixes="rtf doc" xmlns:rtf="http://rtf2xml.sourceforge.net/">


    <doc:stylesheet>
        <doc:name>header.xsl</doc:name>
        <doc:used-by>
            <doc:stylesheet-name>to_tei.xsl</doc:stylesheet-name>
        </doc:used-by>
        <doc:uses/>
        <doc:parameters/>
        <doc:entities>
            <doc:entity name="copyright">Copyright &#x00A9; 2004</doc:entity>
            <doc:entity name="namespace">http://rtf2xml.sourceforge.net/</doc:entity>
            <doc:entity name="current-dtd">http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd</doc:entity>
            <doc:entity name="tei-dtd">http://rtf2xml.sourceforge.net/dtd/teilite.dtd</doc:entity>
        </doc:entities>
        <doc:description>
            <doc:para>&copyright; by Paul Tremblay</doc:para>
            <doc:para>The namespace "rtf" is mapped to "&namespace;".</doc:para>
            <doc:para>This stylesheet creates a TEI.2 header.</doc:para>
        </doc:description>
    </doc:stylesheet>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:preamble</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:description>
            <doc:para>Delete all information in this element.</doc:para>
        </doc:description>
    </doc:template>

    <xsl:template match="rtf:preamble"/>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>make-header</doc:name>
        <doc:type>name</doc:type>
        <doc:parameters/>
        <doc:called-by>
            <doc:template-name>to_tei.xsl#/</doc:template-name>
        </doc:called-by>
        <doc:description>
            <doc:para>Creates the teiHeader element and all the children of that element.</doc:para>
            <doc:para>If the element /rtf:doc/rtf:preamble/rtf:doc-infromation exists, then use
     the information in this element to form the header.</doc:para>
            <doc:para>Otherwise, use the tei-head template, which simply creates default information.</doc:para>
        </doc:description>
    </doc:template>

    <xsl:template name="make-header">
        <teiHeader>
            <xsl:choose>
                <xsl:when test="/rtf:doc/rtf:preamble/rtf:doc-information">
                    <xsl:apply-templates select="/rtf:doc/rtf:preamble/rtf:doc-information" mode="header"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="tei-head"/>
                </xsl:otherwise>
            </xsl:choose>
        </teiHeader>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>tei-head</doc:name>
        <doc:type>name</doc:type>
        <doc:parameters/>
        <doc:called-by>
            <doc:template-name>header.xsl#make-header</doc:template-name>
        </doc:called-by>
        <doc:description>
            <doc:para>Creates default tei header information when there is no rtf:doc-information element.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template name="tei-head">
        <fileDesc>
            <titleStmt>
                <title>
                    <xsl:text>Manuscript</xsl:text>
                </title>
            </titleStmt>
            <publicationStmt>
                <distributor>
                    <xsl:text>none</xsl:text>
                </distributor>
            </publicationStmt>
            <sourceDesc>
                <p>no source--a manuscript</p>
            </sourceDesc>
        </fileDesc>
    </xsl:template>


    <doc:template mode="header" xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:doc-information</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:description>
            <doc:para>Use the rtf:doc-information to build the necessary elements for TEI.2.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match="rtf:doc-information" mode="header">
        <fileDesc>
            <titleStmt>
                <xsl:choose>
                    <xsl:when test="rtf:title">
                        <xsl:apply-templates select="rtf:title" mode="header"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:element name="title">
                            <xsl:text>No Title</xsl:text>
                        </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:choose>
                    <xsl:when test="rtf:author">
                        <xsl:apply-templates select="rtf:author" mode="header"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:element name="author">
                            <xsl:text>No Author</xsl:text>
                        </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>
            </titleStmt>
            <xsl:call-template name="pub-state"/>
        </fileDesc>
        <xsl:call-template name="revision"/>
    </xsl:template>


    <doc:template mode="header" xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:title</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:description>
            <doc:para>Use the rtf:title to build the title element.</doc:para>
        </doc:description>
    </doc:template>
    <xsl:template match="rtf:title" mode="header">
        <title>
            <xsl:apply-templates/>
        </title>
    </xsl:template>


    <doc:template mode="header" xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:author</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:description>
            <doc:para>Use the rtf:author to build the author element.</doc:para>
        </doc:description>
    </doc:template>

    <xsl:template match="rtf:author" mode="header">
        <author>
            <xsl:apply-templates/>
        </author>
    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>pub-state</doc:name>
        <doc:type>name</doc:type>
        <doc:parameters/>
        <doc:called-by>
            <doc:template-name>#rtf:doc-information</doc:template-name>
        </doc:called-by>
        <doc:description>
            <doc:para>Creates default tei header information when there is no rtf:doc-information element.</doc:para>
        </doc:description>
    </doc:template>

    <xsl:template name="pub-state">
        <publicationStmt>
            <distributor>
                <xsl:text>none</xsl:text>
            </distributor>
        </publicationStmt>
        <sourceDesc>
            <p>no source--a manuscript</p>
        </sourceDesc>

    </xsl:template>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>revision</doc:name>
        <doc:type>name</doc:type>
        <doc:parameters/>
        <doc:called-by>
            <doc:template-name>#rtf:doc-information</doc:template-name>
        </doc:called-by>
        <doc:description>
            <doc:para>Creates revision element based on information found in the element creation-time.</doc:para>
        </doc:description>
    </doc:template>

    <xsl:template name="revision">
        <revisionDesc>
            <change>
                <date>
                    <xsl:variable name="year" select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:creation-time/@year"/>
                    <xsl:variable name="month" select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:creation-time/@month"/>
                    <xsl:variable name="day" select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:creation-time/@day"/>
                    <xsl:value-of select="$year"/>
                    <xsl:text>-</xsl:text>
                    <xsl:if test="string-length($month) = 1">
                        <xsl:text>0</xsl:text>
                    </xsl:if>
                    <xsl:value-of select="$month"/>
                    <xsl:text>-</xsl:text>
                    <xsl:if test="string-length($day) = 1">
                        <xsl:text>0</xsl:text>
                    </xsl:if>
                    <xsl:value-of select="$day"/>
                </date>
                <respStmt>
                    <resp>written by</resp>
                    <name>
                        <xsl:call-template name="resp-author"/>
                    </name>
                </respStmt>
                <item>First started document.</item>
            </change>
            <change>
                <date>
                    <xsl:variable name="year" select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:revision-time/@year"/>
                    <xsl:variable name="month" select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:revision-time/@month"/>
                    <xsl:variable name="day" select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:revision-time/@day"/>
                    <xsl:value-of select="$year"/>
                    <xsl:text>-</xsl:text>
                    <xsl:if test="string-length($month) = 1">
                        <xsl:text>0</xsl:text>
                    </xsl:if>
                    <xsl:value-of select="$month"/>
                    <xsl:text>-</xsl:text>
                    <xsl:if test="string-length($day) = 1">
                        <xsl:text>0</xsl:text>
                    </xsl:if>
                    <xsl:value-of select="$day"/>
                </date>
                <respStmt>
                    <resp>written by</resp>
                    <name>
                        <xsl:call-template name="resp-author"/>
                    </name>
                </respStmt>
                <item>Last updated document.</item>
            </change>
        </revisionDesc>

    </xsl:template>


    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>resp-author</doc:name>
        <doc:type>name</doc:type>
        <doc:parameters/>
        <doc:called-by>
            <doc:template-name>#revision</doc:template-name>
        </doc:called-by>
        <doc:description>
            <doc:para>Use author information to make a resp-author element.</doc:para>
        </doc:description>
    </doc:template>

    <xsl:template name="resp-author">
        <xsl:choose>
            <xsl:when test="descendant::rtf:author">
                <xsl:apply-templates select="rtf:author" mode="resp-author"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <doc:template mode="resp-author" xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>rtf:author</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:description>
            <doc:para>Copy information in this element to make a resp-author element.</doc:para>
        </doc:description>
    </doc:template>

    <xsl:template match="rtf:author" mode="resp-author">
        <xsl:apply-templates/>
    </xsl:template>

</xsl:stylesheet>
