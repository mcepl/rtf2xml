<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY copyright "Copyright &#x00A9; 2004">
    <!ENTITY namespace "http://rtf2xml.sourceforge.net/">
    <!ENTITY current-dtd "http://rtf2xml.sourceforge.net/dtd/rtf2xml.6.dtd">
    <!ENTITY tei-dtd "http://rtf2xml.sourceforge.net/dtd/teilite.dtd">
]><!--
#########################################################################
#                                                                       #
#                                                                       #
#   copyright 2002 Paul Henry Tremblay                                  #
#                                                                       #
#   This program is distributed in the hope that it will be useful,     #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of      #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU    #
#   General Public License for more details.                            #
#                                                                       #
#   You should have received a copy of the GNU General Public License   #
#   along with this program; if not, write to the Free Software         #
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA            #
#   02111-1307 USA                                                      #
#                                                                       #
#                                                                       #
#########################################################################
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:doc="http://www.kielen.com/documentation" exclude-result-prefixes="rtf doc" xmlns:rtf="http://rtf2xml.sourceforge.net/">

    <xsl:include href="header.xsl"/>
    <xsl:include href="blocks.xsl"/>
    <xsl:include href="lists.xsl"/>
    <xsl:include href="para.xsl"/>
    <xsl:include href="inline.xsl"/>
    <xsl:include href="tables.xsl"/>
    <xsl:include href="fields.xsl"/>

    <doc:stylesheet>
        <doc:name>to_tei.xsl</doc:name>
        <doc:used-by/>
        <doc:uses>
            <doc:stylesheet-name>header.xsl</doc:stylesheet-name>
            <doc:stylesheet-name>blocks.xsl</doc:stylesheet-name>
            <doc:stylesheet-name>lists.xsl</doc:stylesheet-name>
            <doc:stylesheet-name>para.xsl</doc:stylesheet-name>
            <doc:stylesheet-name>tables.xsl</doc:stylesheet-name>
            <doc:stylesheet-name>fields.xsl</doc:stylesheet-name>
        </doc:uses>
        <doc:parameters/>
        <doc:entities>
            <doc:entity name="copyright">Copyright &#x00A9; 2004</doc:entity>
            <doc:entity name="namespace">http://rtf2xml.sourceforge.net/</doc:entity>
            <doc:entity name="current-dtd">http://rtf2xml.sourceforge.net/dtd/rtf2xml.53.dtd</doc:entity>
            <doc:entity name="tei-dtd">http://rtf2xml.sourceforge.net/dtd/teilite.dtd</doc:entity>
        </doc:entities>
        <doc:description>
            <doc:para>&copyright; by Paul Tremblay</doc:para>
            <doc:para>The namespace "rtf" is mapped to "&namespace;".</doc:para>
            <doc:para>This stylesheet converts raw XML generated from the script rtf2xml to TEI.2 that conforms to the dtd at  &tei-dtd;.</doc:para>
        </doc:description>
    </doc:stylesheet>



    <xsl:output
    method = "xml"
    doctype-system = "/home/paul/cvs/rtf2xml/dtd/teilite.dtd"
    />


    <!--
    <xsl:output 
    method = "xml" 
    doctype-system = "http://rtf2xml.sourceforge.net/dtd/teilite.dtd"
    doctype-public = ""
    />
    -->

    <doc:variables xmlns:doc="http://www.kielen.com/documentation">			
        <doc:description>			   
            <doc:para>delete-list-text: Set to true if you want to delelte list-text in paragraphs.</doc:para>			
            <doc:para>delete-field-blocks: Set to true if you want to delelte indices, table-of-contents, and table-of-authories.</doc:para>			
            <doc:para>delete-annotation: Set to true if you want to delelte comments 
            or annotation. Set to false if you want annotations to appear in the element
            &lt;note rend="annotation"&gt;.</doc:para>			
        </doc:description>		
    </doc:variables>	
    <xsl:variable name = "delete-list-text">true</xsl:variable>
    <xsl:variable name = "delete-field-blocks">true</xsl:variable>
    <xsl:variable name = "delete-annotation">false</xsl:variable>

    <doc:template xmlns:doc="http://www.kielen.com/documentation">
        <doc:name>/</doc:name>
        <doc:type>match</doc:type>
        <doc:parameters/>
        <doc:called-by/>
        <doc:calls>
            <doc:template-name>#make-header</doc:template-name>
        </doc:calls>
        <doc:description>
            <doc:para>Matches root element and applies all other templates.</doc:para>
        </doc:description>
    </doc:template>

    <xsl:template match="/">
        <TEI.2>
            <xsl:call-template name="make-header"/>
            <xsl:apply-templates/>
        </TEI.2>
    </xsl:template>

</xsl:stylesheet>
