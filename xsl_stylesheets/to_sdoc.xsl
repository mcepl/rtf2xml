
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:rtf="http://rtf2xml.sourceforge.net/" exclude-result-prefixes="rtf" version="1.0">
<!--
    <xsl:output method="xml" doctype-system="http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" doctype-public="-//OASIS//DTD DocBook XML V4.1.2//EN"/>
    -->
    <xsl:output method="xml" doctype-system="/home/paul/Documents/data/dtds/sdocbook.dtd"/>

    <xsl:param name="keep-list-text">false</xsl:param>

    <!--Get rid of toc, index, toa-->
    <xsl:param name="keep-index">false</xsl:param>

    <xsl:template match="rtf:doc">
        <article>
            <xsl:apply-templates select="rtf:preamble"/>
            <xsl:apply-templates select="rtf:body"/>
        </article>
    </xsl:template>

    <xsl:template match="rtf:preamble">
        <xsl:choose>
            <xsl:when test="rtf:doc-information">
                <articleinfo>
                    <xsl:apply-templates select="rtf:doc-information"/>
                </articleinfo>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="rtf:doc-information">
        <xsl:apply-templates select="rtf:author"/>
        <xsl:apply-templates select="rtf:title"/>
        <xsl:call-template name="revision"/>
    </xsl:template>

    <xsl:template match="rtf:title">
        <title>
            <xsl:apply-templates/>
        </title>
    </xsl:template>

    <xsl:template match="rtf:author">
        <author>
            <firstname>
                <xsl:value-of select="substring-before(., ' ')"/>
            </firstname>
            <surname>
                <xsl:value-of select="substring-after(., ' ')"/>
            </surname>
        </author>

    </xsl:template>

    <xsl:template name="revision">
        <revhistory>
            <revision>
                <revnumber>1.0</revnumber>
                <date>
                    <xsl:value-of select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:creation-time/@day"/>
                    <xsl:text> </xsl:text>
                    <xsl:call-template name="month">
                        <xsl:with-param name="num">
                            <xsl:value-of select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:creation-time/@month"/>
                        </xsl:with-param>
                    </xsl:call-template>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:creation-time/@year"/>
                </date>
                <revremark>First started document.</revremark>
            </revision>
            <revision>
                <revnumber>1</revnumber>
                <date>
                    <xsl:value-of select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:revision-time/@day"/>
                    <xsl:text> </xsl:text>
                    <xsl:call-template name="month">
                        <xsl:with-param name="num">
                            <xsl:value-of select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:revision-time/@month"/>
                        </xsl:with-param>
                    </xsl:call-template>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="/rtf:doc/rtf:preamble/rtf:doc-information/rtf:revision-time/@year"/>
                </date>
                <revremark>Last updated document.</revremark>
            </revision>
        </revhistory>

    </xsl:template>

    <xsl:template name="month">
        <xsl:param name="num"></xsl:param>
        <xsl:choose>
            <xsl:when test="$num='1'">
                <xsl:text>January</xsl:text>
            </xsl:when>
            <xsl:when test="$num='2'">
                <xsl:text>February</xsl:text>
            </xsl:when>
            <xsl:when test="$num = '3'">
                <xsl:text>March</xsl:text>
            </xsl:when>
            <xsl:when test="$num='4'">
                <xsl:text>April</xsl:text>
            </xsl:when>
            <xsl:when test="$num='5'">
                <xsl:text>May</xsl:text>
            </xsl:when>
            <xsl:when test="$num='6'">
                <xsl:text>June</xsl:text>
            </xsl:when>
            <xsl:when test="$num='7'">
                <xsl:text>July</xsl:text>
            </xsl:when>
            <xsl:when test="$num='8'">
                <xsl:text>August</xsl:text>
            </xsl:when>
            <xsl:when test="$num='9'">
                <xsl:text>September</xsl:text>
            </xsl:when>
            <xsl:when test="$num='10'">
                <xsl:text>October</xsl:text>
            </xsl:when>
            <xsl:when test="$num='11'">
                <xsl:text>November</xsl:text>
            </xsl:when>
            <xsl:when test="$num='12'">
                <xsl:text>December</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <!--turn group-style with name of heading 1, etc, to title-->
    <!--Check if the style-group is the child of a heading section. If so, create a 
        title element. Othewise, simply process the template.
    -->
    <xsl:template match="rtf:style-group[@name = 'heading 1']|rtf:style-group[@name = 'heading 2']|rtf:style-group[@name = 'heading 3']|rtf:style-group[@name = 'heading 4']|rtf:style-group[@name = 'heading 5']|rtf:style-group[@name = 'heading 6']|rtf:style-group[@name = 'heading 7']|rtf:style-group[@name = 'heading 8']|rtf:style-group[@name = 'heading 9']">
        <xsl:variable name="heading" select="@name"/>
        <xsl:choose>
            <!--
            <xsl:when test="ancestor::rtf:section[@type = 'heading 1']|ancestor::rtf:section[@type = 'heading 2']|ancestor::rtf:section[@type = 'heading 3']|ancestor::rtf:section[@type = 'heading 4']|ancestor::rtf:section[@type = 'heading 5']|ancestor::rtf:section[@type = 'heading 6']|ancestor::rtf:section[@type = 'heading 7']|ancestor::rtf:section[@type = 'heading 8']|ancestor::rtf:section[@type = 'heading 9']">
            -->
            <xsl:when test="parent::rtf:section[@type = $heading]">
                <title>
                    <xsl:apply-templates/>
                </title>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>



    <xsl:template match="rtf:para">
        <xsl:if test="normalize-space(.)">
            <xsl:variable name="style">
                <xsl:value-of select="../@name"/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="($style = 'heading 1')or ($style = 'heading 2')or ($style = 'heading 3')or ($style = 'heading 4')or ($style = 'heading 5')or ($style = 'heading 6')or ($style = 'heading 7')or ($style = 'heading 8')or ($style = 'heading 9')">
                    <xsl:choose>
                        <xsl:when test="../../parent::rtf:section[@type = $style]">
                            <xsl:call-template name="para"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="para">
                                <xsl:attribute name="role">
                                    <xsl:value-of select="../@name"/>
                                </xsl:attribute>
                                <xsl:call-template name="para"/>
                            </xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:element name="para">
                        <xsl:attribute name="role">
                            <xsl:value-of select="../@name"/>
                        </xsl:attribute>
                        <xsl:call-template name="para"/>
                    </xsl:element>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:template name="para">
        <xsl:choose>
            <xsl:when test="@italics = 'true'">
                <emphasis role="paragraph-emphasis-italics">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@bold = 'true'">
                <emphasis role="paragraph-emphasis-bold">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@underlined">
                <emphasis role="paragraph-emphasis-underlined">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="(@strike-through = 'true')or (@double-strike-through = 'true')or (@emboss = 'true')or (@engrave = 'true')or (@small-caps = 'true')or (@shadow = 'true')or (@hidden = 'true')or (@outline = 'true')">
                <emphasis role="paragraph-emphasis">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <!--TABLE-->

    <!--
    <xsl:template match = "table">
        <xsl:element name = "table">
            <title/>
            <xsl:element name = "tgroup">
                <xsl:attribute name = "cols">
                    <xsl:value-of select = "@number-of-columns"/>
                </xsl:attribute>
                <xsl:if test = "row[@header = 'true']">
                    <xsl:element name = "thead">
                        <xsl:apply-templates select = "row[@header = 'true']" mode = "header"/>
                    </xsl:element>
                </xsl:if>
                <xsl:element name = "tbody">
                    <xsl:apply-templates select = "not(row[@header = 'true'])"/>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    -->
    <xsl:template match="rtf:table">
        <xsl:element name="table">
            <title/>
            <xsl:element name="tgroup">
                <xsl:attribute name="cols">
                    <xsl:value-of select="@number-of-columns"/>
                </xsl:attribute>
                <xsl:element name="tbody">
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <xsl:template match="rtf:row">
        <xsl:choose>
            <xsl:when test="normalize-space(.)">
                <xsl:element name="row">
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
        </xsl:choose>
    </xsl:template>


    <xsl:template match="rtf:row[@header = 'true']" mode="header">
        <xsl:choose>
            <xsl:when test="normalize-space(.)">
                <xsl:element name="row">
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="rtf:cell">
        <xsl:element name="entry">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>


    <xsl:template match="rtf:footnote">
        <footnote>
            <xsl:apply-templates/>
        </footnote>
    </xsl:template>

    <xsl:template match="rtf:list[@type = 'ordered']">
        <xsl:if test="normalize-space(.)">

            <orderedlist>
                <xsl:apply-templates/>
            </orderedlist>
        </xsl:if>
    </xsl:template>

    <xsl:template match="rtf:list[@type = 'unordered']">
        <xsl:if test="normalize-space(.)">

            <itemizedlist>
                <xsl:apply-templates/>
            </itemizedlist>
        </xsl:if>
    </xsl:template>



    <xsl:template match="rtf:item">
        <xsl:if test="normalize-space(.)">
            <listitem>
                <xsl:apply-templates/>
            </listitem>
        </xsl:if>
    </xsl:template>

    <xsl:template match="rtf:list-text_">
        <xsl:choose>
            <xsl:when test="$keep-list-text = 'true'">
                <xsl:apply-templates/>
            </xsl:when>
        </xsl:choose>

    </xsl:template>

    <xsl:template match="rtf:list-text">
        <xsl:choose>
            <xsl:when test="ancestor::rtf:list">

            </xsl:when>
            <xsl:when test="$keep-list-text = 'true'">
                <xsl:apply-templates/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="rtf:section">
        <xsl:choose>
            <xsl:when test="normalize-space(.)">
                <xsl:choose>
                    <xsl:when test="rtf:field-block[@type = 'index']or rtf:field-block[@type = 'toc']or rtf:field-block[@type = 'toa']">
                        <xsl:if test="$keep-index = 'true'">
                            <xsl:element name="section">
                                <xsl:attribute name="role">
                                    <xsl:value-of select="field-block/@type"/>
                                </xsl:attribute>
                                <title/>
                                <xsl:apply-templates/>
                            </xsl:element>
                        </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="@type='rtf-native'">
                                <section>
                                    <title/>
                                    <xsl:apply-templates/>
                                </section>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:element name="section">
                                    <xsl:apply-templates/>
                                </xsl:element>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!--paragraph-definition to other elements-->
    <!--

    <xsl:template match = "paragraph-definition">
        <xsl:choose>
            <xsl:when test = "(@name = 'quote') or (@name = 'blockquote')">
                <xsl:element name = "blockquote">
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    -->


    <xsl:template match="rtf:wrap-paragraph-definition">
        <xsl:choose>
            <xsl:when test="(@name = 'quote') or (@name = 'blockquote') or (@name = 'Blockquote')">
                <xsl:element name="blockquote">
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <xsl:when test="(@name = 'heading 1')or (@name = 'heading 2')or (@name = 'heading 3')or (@name = 'heading 4')or (@name = 'heading 5')or (@name = 'heading 6')">
                <xsl:variable name="style">
                    <xsl:value-of select="@name"/>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="ancestor::rtf:section[@type = $style]">
                        <xsl:element name="title">
                            <xsl:apply-templates/>
                        </xsl:element>
                        <xsl:choose>
                            <xsl:when test="descendant::rtf:style-group[not(@name = $style)]"></xsl:when>
                            <xsl:otherwise>
                                <para/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>

                    <xsl:otherwise>
                        <xsl:apply-templates/>
                    </xsl:otherwise>
                </xsl:choose>

            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--
    INLINE ITEMS
    -->
    <xsl:template match="rtf:inline">
        <xsl:choose>
            <xsl:when test="normalize-space(.)">
                <xsl:call-template name="inline"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="inline">
        <xsl:choose>
            <xsl:when test="@footnote-marker"/>
            <xsl:when test="@character-style">
                <xsl:element name="phrase">
                    <xsl:attribute name="role">
                        <xsl:value-of select="@character-style"/>
                    </xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <xsl:when test="(@italics = 'true') and (@bold = 'true') and (@underlined)">
                <emphasis role="bold-italic-underlined">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="(@italics = 'true') and (@bold = 'true')">
                <emphasis role="bold-italic">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="(@italics = 'true') and (@underlined)">
                <emphasis role="italic-underlined">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="(@bold = 'true') and (@underlined)">
                <emphasis role="bold-underlined">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@italics = 'true'">
                <emphasis role="italics">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@bold = 'true'">
                <emphasis role="bold">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@underlined">
                <xsl:choose>
                    <xsl:when test="@underlined = 'false'">
                        <xsl:call-template name="inline-off"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <emphasis role="underlined">
                            <xsl:apply-templates/>
                        </emphasis>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="@strike-through = 'true'">
                <emphasis role="strike-through">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@double-strike-through = 'true'">
                <emphasis role="double-strike-through">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@superscript = 'true'">
                <emphasis role="superscript">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@subscript = 'true'">
                <emphasis role="subscript">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@shadow = 'true'">
                <emphasis role="shadow">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@outline = 'true'">
                <emphasis role="outline">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@emboss = 'true'">
                <emphasis role="emboss">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@font-color = 'true'">
                <xsl:element name="emphasis">
                    <xsl:attribute name="role">
                        <xsl:text>font-color:</xsl:text>
                        <xsl:value-of select="@font-color"/>
                    </xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <xsl:when test="@engrave = 'true'">
                <emphasis role="engrave">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@small-caps = 'true'">
                <emphasis role="small-caps">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@caps = 'true'">
                <emphasis role="caps">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@hidden = 'true'">
                <emphasis role="hidden">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:when test="@font-size">
                <xsl:element name="emphasis">
                    <xsl:attribute name="role">
                        <xsl:text>font-size:</xsl:text>
                        <xsl:value-of select="@font-size"/>
                    </xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <xsl:when test="@font-style">
                <xsl:element name="emphasis">
                    <xsl:attribute name="role">
                        <xsl:text>font-style:</xsl:text>
                        <xsl:value-of select="@font-style"/>
                    </xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="inline-off"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--handle off inline-->
    <xsl:template name="inline-off">
        <xsl:choose>
            <xsl:when test="(../@italics = 'true') or (../@bold = 'true') or (../@strike-through = 'true') or (../@underlined) or (../@strike-through = 'true')or (../@double-strike-through = 'true')or (../@emboss = 'true')or (../@shadow = 'true')or (../@outline = 'true')or (../@caps = 'true')or (../@small-caps = 'true')or (../@hidden = 'true')">
                <emphasis role="paragraph-emphasis-off">
                    <xsl:apply-templates/>
                </emphasis>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <!--Fields-->

    <xsl:template match="rtf:field">
        <xsl:choose>
            <xsl:when test="@type = 'index-entry'">
                <xsl:element name="phrase">
                    <xsl:attribute name="role">
                        <xsl:text>index-entry:</xsl:text>
                        <xsl:value-of select="@main-entry"/>
                    </xsl:attribute>
                </xsl:element>
            </xsl:when>
        </xsl:choose>
    </xsl:template>


    <!--headers and footers-->

    <xsl:template match="rtf:header-or-footer"/>

</xsl:stylesheet>


<!--









-->


