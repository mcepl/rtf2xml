<!--
Converts valid XML to RTf
-->
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  version="1.0" 
  xmlns:rtf="http://rtf2xml.sourceforge.net/"
  xmlns:doc="http://ww.kielen.com/documentation"
  
  
  >


	<doc:stylesheet xmlns:doc="http://www.kielen.com/documentation">
		<doc:name>xml2rtf.xsl</doc:name>
		<doc:used-by/>
		<doc:uses/>
		<doc:parameters/>
		<doc:entities/>
		<doc:description>
		   <doc:para>This stylesheet converts XML conforming to "" to RTF.</doc:para>
		</doc:description>
	</doc:stylesheet>

<xsl:output method="text"/>

<xsl:key name="font-numbers" match="rtf:font-in-table" use="@name"/>

<xsl:key name="style-numbers" match="rtf:paragraph-style-in-table" use="@name"/>
<xsl:key name="char-style-numbers" match="rtf:character-style-in-table" use="@name"/>

<xsl:key name="color-numbers" match="rtf:color-in-table" use="@value"/>
<xsl:key name="list" match="rtf:list-in-table" use="@list-table-id" />
<xsl:key name="override" match="rtf:override-list" use="@list-id" />
<xsl:include href = "../common/dec2hex.xsl"/>
<xsl:include href = "xml2rtf_lists.xsl"/>

<!--The root element doc needs an opening and closing bracket.
It also needs a code page. I use 1252 for convenience's sake
-->
    <xsl:template match="rtf:doc">
        <xsl:text>{\rtf1\ansi\ansicpg1252\uc1&#xA;</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>&#xA;}</xsl:text>
    </xsl:template>

    <xsl:template match="rtf:rtf-definition">
        <xsl:choose>
            <xsl:when test="@default-font = 'Not Defined'"/>
            <xsl:otherwise>
                <xsl:text>\deff</xsl:text>
                <xsl:value-of select="key('font-numbers', @default-font)/@num"/>

            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

<!--The font table requires an opening and closing bracket and the
words \fonttbl
-->
    <xsl:template match="rtf:font-table">
        <xsl:text>{\fonttbl&#xA;</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}&#xA;</xsl:text>
    </xsl:template>


<!--
<font-in-table name="Times" num="4"/>

font-in-table requires an opening and closing bracket.
The final result should look like:
{\f1 Times;}
-->

    <xsl:template match="rtf:font-in-table">
        <xsl:text>{\f</xsl:text>
        <xsl:value-of select="@num"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="@name"/>
        <xsl:text>;}&#xA;</xsl:text>

    </xsl:template>

<!--
<color-table>

The final result should look like this:
{\colortbl; [individual colors] }}
-->
    <xsl:template match="rtf:color-table">
        <xsl:text>{\colortbl; &#xA;</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>&#xA;}&#xA;</xsl:text>
    </xsl:template>


<!--
<color-in-table num="1" value="#000000"/>

The final result should look like:
\red0\green0\blue0;


-->

    <xsl:template match="rtf:color-in-table">
        <xsl:text>\red</xsl:text>
        <xsl:call-template name="convert-hex">
            <xsl:with-param name="num" select="substring(@value,2,2)"/>
        </xsl:call-template>
        <xsl:text>\green</xsl:text>
        <xsl:call-template name="convert-hex">
            <xsl:with-param name="num" select="substring(@value,4,2)"/>
        </xsl:call-template>
        <xsl:text>\blue</xsl:text>
        <xsl:call-template name="convert-hex">
            <xsl:with-param name="num" select="substring(@value,6,2)"/>
        </xsl:call-template>
        <xsl:text>;&#xA;</xsl:text>
    </xsl:template>

<!--
<style-table>

Should look like:

{\stylesheet
[individual styles]
}

-->

    <xsl:template match="rtf:style-table">
        <xsl:text>{\stylesheet</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}&#xA;</xsl:text>
        <xsl:choose>
            <xsl:when test="../rtf:list-table"></xsl:when>
            <xsl:otherwise>
                <!--
                <xsl:call-template name="list-table"></xsl:call-template>
                -->

            </xsl:otherwise>

        </xsl:choose>
    </xsl:template>


    <!--
\*\cs10
-->

    <xsl:template match="rtf:doc-information">
        <xsl:text>{\info&#xA;</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}&#xA;</xsl:text>
    </xsl:template>

    <xsl:template match="rtf:doc-information/rtf:title">
        <xsl:text>{\title </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}&#xA;</xsl:text>
    </xsl:template>

    <xsl:template match="rtf:doc-information/rtf:author">
        <xsl:text>{\author </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}&#xA;</xsl:text>
    </xsl:template>

    <xsl:template match="rtf:doc-information/rtf:operator">
        <xsl:text>{\operator </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}&#xA;</xsl:text>
    </xsl:template>



    <xsl:template match="rtf:page-definition">
        <xsl:if test="@margin-right">
            <xsl:text>\margl</xsl:text>
            <xsl:value-of select="round(@margin-right * 20)"/>
            <xsl:text>&#xA;</xsl:text>
        </xsl:if>

        <xsl:if test="@margin-right">
            <xsl:text>\margr</xsl:text>
            <xsl:value-of select="round(@margin-right * 20)"/>
            <xsl:text>&#xA;</xsl:text>
        </xsl:if>

        <xsl:if test="@margin-bottom">
            <xsl:text>\margb</xsl:text>
            <xsl:value-of select="round(@margin-bottom * 20)"/>
            <xsl:text>&#xA;</xsl:text>
        </xsl:if>

        <xsl:if test="@margin-top">
            <xsl:text>\margt</xsl:text>
            <xsl:value-of select="round(@margin-top * 20)"/>
            <xsl:text>&#xA;</xsl:text>
        </xsl:if>

        <xsl:if test="@gutter">
            <xsl:text>\gutter</xsl:text>
            <xsl:value-of select="round(@gutter * 20)"/>
            <xsl:text>&#xA;</xsl:text>
        </xsl:if>

        <!--
        <xsl:text>\sectd&#xA;</xsl:text>
        -->
    </xsl:template>

    <xsl:template match="rtf:body">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="rtf:section[@type = 'rtf-native']">
        <xsl:choose>
            <xsl:when test="@num = '1'">

            </xsl:when>
            <xsl:otherwise>
                <xsl:text>\sect</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>\sectd</xsl:text>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="rtf:paragraph-definition">
        <xsl:if test = "descendant::rtf:list-text">
            <xsl:text>{\listtext\pard\plain</xsl:text>
            <xsl:if test = "@left-indent">
                    <xsl:text>\li</xsl:text>
                    <xsl:value-of select = "round(@left-indent * 20)"/>
                </xsl:if>
            <xsl:if test = "@first-line-indent">
                    <xsl:text>\fi</xsl:text>
                    <xsl:value-of select = "round(@first-line-indent * 20)"/>
            </xsl:if>

            <xsl:variable name = "tb-id">
                <xsl:value-of select = "key('override', @list-id)/@list-table-id"/>
            </xsl:variable>
            <xsl:variable name = "level" select = "@list-level + 1"/>
            <xsl:if test = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@bold">
                <xsl:text>\b</xsl:text>
            </xsl:if>
            <xsl:if test = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@italics">
                <xsl:text>\i</xsl:text>
            </xsl:if>
            <xsl:apply-templates select = "descendant::rtf:list-text" mode = "before-pard"/>
            <xsl:text>}</xsl:text>
        </xsl:if>
        <xsl:text>\pard</xsl:text>
        <xsl:text>\s</xsl:text>
        <xsl:value-of select="key('style-numbers', @name)/@num"/>
        <xsl:if test="ancestor::rtf:table">
            <xsl:text>\intbl</xsl:text>
        </xsl:if>
        <xsl:call-template name="paragraph-info"/>
        <xsl:if test="@tabs">
            <xsl:call-template name="tabs">
                <xsl:with-param name="tabs-string" select="@tabs"/>
            </xsl:call-template>
        </xsl:if>


        <xsl:choose>
            <xsl:when test="parent::rtf:border-group">
                <xsl:call-template name="paragraph-border"/>
            </xsl:when>
        </xsl:choose>

        <xsl:text> </xsl:text>
        <xsl:apply-templates/>

    </xsl:template>

    <xsl:template match="rtf:para">
        <xsl:apply-templates/>
        <xsl:text>\par&#xA;</xsl:text>
    </xsl:template>



    <xsl:template match = "rtf:list-text" mode = "before-pard">
        <xsl:call-template name="character-info"/>
        <xsl:text> </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="rtf:cell/rtf:paragraph-definition[last()]/rtf:para[last()]|rtf:cell/rtf:style-group/rtf:paragraph-definition[last()]/rtf:para[last()]|rtf:cell/rtf:style-group/rtf:border-group/rtf:paragraph-definition[last()]/rtf:para[last()]|rtf:cell/rtf:border-group/rtf:paragraph-definition[last()]/rtf:para[last()]">

    <!--
    <xsl:template match="rtf:cell//rtf:para[last()]">
    -->

        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="rtf:inline">
        <xsl:text>{</xsl:text>
        <xsl:call-template name="character-info"/>
        <xsl:text> </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}</xsl:text>
    </xsl:template>

<!--
Template converts character info to RTF.

<inline hidden = "true"> => \v
<paragraph-in-table hidden = "true"> => \v
-->

    <xsl:template name="paragraph-info">

        <xsl:if test="@nest-level">
            <xsl:text>\itap</xsl:text>
            <xsl:value-of select="@nest-level"/>
        </xsl:if>
        <xsl:if test="@widow-control = 'true'">
            <xsl:text>\widctlpar</xsl:text>
        </xsl:if>
        <xsl:if test="@widow-control = 'false'">
            <xsl:text>\nowidctlpar</xsl:text>
        </xsl:if>
        <xsl:if test="@adjust-right">
            <xsl:text>\adjustright</xsl:text>
        </xsl:if>
        <xsl:if test="@language">
            <xsl:text>\lang</xsl:text>
            <xsl:value-of select="@language"/>
        </xsl:if>
        <xsl:if test="@right-indent">
            <xsl:text>\ri</xsl:text>
            <xsl:value-of select="round(@right-indent * 20)"/>
        </xsl:if>
        <xsl:if test="@left-indent">
            <xsl:text>\li</xsl:text>
            <xsl:value-of select="round(@left-indent * 20)"/>
        </xsl:if>
        <xsl:if test="@first-line-indent">
            <xsl:text>\fi</xsl:text>
            <xsl:value-of select="round(@first-line-indent * 20)"/>
        </xsl:if>
        <xsl:if test="@space-before">
            <xsl:text>\sb</xsl:text>
            <xsl:value-of select="round(@space-before * 20)"/>
        </xsl:if>
        <xsl:if test="@space-after">
            <xsl:text>\sa</xsl:text>
            <xsl:value-of select="round(@space-after * 20)"/>
        </xsl:if>
        <xsl:if test="@line-spacing">
            <xsl:text>\sl</xsl:text>
            <xsl:value-of select="round(@line-spacing * 20)"/>
        </xsl:if>
        <xsl:if test="@list-id">
            <xsl:text>\ls</xsl:text>
            <xsl:value-of select="@list-id"/>
        </xsl:if>
        <xsl:if test="@list-level">
            <xsl:text>\ilvl</xsl:text>
            <xsl:value-of select="@list-level"/>
        </xsl:if>
        <xsl:if test="@align = 'left'">
            <xsl:text>\ql</xsl:text>
        </xsl:if>
        <xsl:if test="@align = 'right'">
            <xsl:text>\qr</xsl:text>
        </xsl:if>
        <xsl:if test="@align = 'center'">
            <xsl:text>\qc</xsl:text>
        </xsl:if>
        <xsl:if test="@align = 'justified'">
            <xsl:text>\qj</xsl:text>
        </xsl:if>
        <xsl:if test="@based-on-style">
            <xsl:text>\sbasedon</xsl:text>
            <xsl:value-of select="key('style-numbers', @based-on-style)/@num"/>
        </xsl:if>
        <xsl:if test="@next-style">
            <xsl:text>\snext</xsl:text>
            <xsl:value-of select="key('style-numbers', @next-style)/@num"/>
        </xsl:if>

    </xsl:template>

    <xsl:template name="tabs">
        <xsl:param name="tabs-string"></xsl:param>
        <xsl:call-template name="parse-tabs-strings">
            <xsl:with-param name="the-tabs-string" select="substring-before($tabs-string, ';')"/>
        </xsl:call-template>
        <xsl:variable name="remainder" select="substring-after($tabs-string, ';')"/>
        <xsl:if test="normalize-space($remainder)">
            <xsl:call-template name="tabs">
                <xsl:with-param name="tabs-string" select="$remainder"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xsl:template name="parse-tabs-strings">
        <xsl:param name="the-tabs-string"></xsl:param>
        <xsl:if test="normalize-space($the-tabs-string)">
            <xsl:variable name="leader" select="substring-before($the-tabs-string, '^')"/>
            <xsl:choose>
                <xsl:when test="normalize-space($leader)">
                    <xsl:call-template name="leader">
                        <xsl:with-param name="the-leader" select="$leader"/>
                    </xsl:call-template>
                    <xsl:variable name="the-tabs-string2" select="substring-after($the-tabs-string, '^')"/>
                    <xsl:variable name="position2" select="substring-before($the-tabs-string2, ':')"/>
                    <xsl:call-template name="position">
                        <xsl:with-param name="the-position" select="$position2"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:variable name="position" select="substring-before($the-tabs-string, ':')"/>
                    <xsl:call-template name="position">
                        <xsl:with-param name="the-position" select="$position"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:variable name="the-tabs-string3" select="substring-after($the-tabs-string, ':')"/>
            <xsl:text>\tx</xsl:text>
            <xsl:value-of select="round($the-tabs-string3 * 20)"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="leader">
        <xsl:param name="the-leader"></xsl:param>
        <xsl:choose>
            <xsl:when test="$the-leader = 'leader-underline'">
                <xsl:text>\tlul</xsl:text>
            </xsl:when>
            <xsl:when test="$the-leader = 'leader-hyphen'">
                <xsl:text>\tlhyph</xsl:text>
            </xsl:when>
            <xsl:when test="$the-leader = 'leader-dot'">
                <xsl:text>\tldot</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="position">
        <xsl:param name="the-position"></xsl:param>

        <xsl:choose>
            <xsl:when test="$the-position = 'left'"></xsl:when>
            <xsl:when test="$the-position = 'right'">
                <xsl:text>\tqr</xsl:text>
            </xsl:when>
            <xsl:when test="$the-position = 'center'">
                <xsl:text>\tqc</xsl:text>
            </xsl:when>
            <xsl:when test="$the-position = 'decimal'">
                <xsl:text>\tqdec</xsl:text>
            </xsl:when>
            <xsl:when test="$the-position = 'bar'">
                <xsl:text>\tb</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>





    <xsl:template name="character-info">
        <xsl:param name="inline">0</xsl:param>
        <xsl:if test = "@annotation = 'true'">
            <xsl:text>\*\annotation</xsl:text>
        </xsl:if>
        <xsl:if test="@character-style">
            <xsl:text>\cs</xsl:text>
            <xsl:value-of select="key('char-style-numbers', @character-style)/@num"/>
        </xsl:if>
        <xsl:if test="@hidden">
            <xsl:text>\v</xsl:text>
        </xsl:if>
        <xsl:if test="@bold='true'">
            <xsl:text>\b</xsl:text>
        </xsl:if>
        <xsl:if test="@bold = 'false'">
            <xsl:text>\b0</xsl:text>
        </xsl:if>
        <xsl:if test="@italics = 'true'">
            <xsl:text>\i</xsl:text>
        </xsl:if>
        <xsl:if test="@italics = 'false'">
            <xsl:text>\i0</xsl:text>
        </xsl:if>
        <xsl:if test="@strike-through = 'true'">
            <xsl:text>\strike</xsl:text>
        </xsl:if>
        <xsl:if test="@shadow = 'true'">
            <xsl:text>\shad</xsl:text>
        </xsl:if>
        <xsl:if test="@outline = 'true'">
            <xsl:text>\outl</xsl:text>
        </xsl:if>
        <xsl:if test="@caps = 'true'">
            <xsl:text>\caps</xsl:text>
        </xsl:if>
        <xsl:if test="@small-caps = 'true'">
            <xsl:text>\scaps</xsl:text>
        </xsl:if>
        <xsl:if test="@double-strike-through = 'true'">
            <xsl:text>\striked</xsl:text>
        </xsl:if>
        <xsl:if test="@font-color">
            <xsl:text>\cf</xsl:text>
            <xsl:value-of select="key('color-numbers', @font-color)/@num"/>
        </xsl:if>
        <xsl:if test="@font-style">
            <xsl:text>\f</xsl:text>
            <xsl:value-of select="key('font-numbers', @font-style)/@num"/>
        </xsl:if>
        <xsl:if test="@font-size">
            <xsl:text>\fs</xsl:text>
            <xsl:value-of select="@font-size * 2"/>
        </xsl:if>

        <xsl:if test="@based-on-style">
            <xsl:text>\sbasedon</xsl:text>
            <xsl:value-of select="key('char-style-numbers', @based-on-style)/@num"/>
        </xsl:if>
        <xsl:if test="@next-style">
            <xsl:text>\snext</xsl:text>
            <xsl:value-of select="key('char-style-numbers', @next-style)/@num"/>
        </xsl:if>
        <xsl:if test="@superscript">
            <xsl:choose>
                <xsl:when test="@superscript = 'true'">
                    <xsl:text>\super</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>\up</xsl:text>
                    <xsl:value-of select="@superscript * 2"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
        <xsl:if test="@subscript">
            <xsl:choose>
                <xsl:when test="@subscript = 'true'">
                    <xsl:text>\sub</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>\dn</xsl:text>
                    <xsl:value-of select="@subscript * 2"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
        <xsl:if test="@plain = 'true'">
            <xsl:text>\plain</xsl:text>
        </xsl:if>
        <xsl:if test="@emboss = 'true'">
            <xsl:text>\embo</xsl:text>
        </xsl:if>
        <xsl:if test="@engrave = 'true'">
            <xsl:text>\impr</xsl:text>
        </xsl:if>
        <xsl:if test="@underlined">
            <xsl:call-template name="underline"/>
        </xsl:if>
        <!--for comments which look like:
            <inline annotation = 'true'>
        -->



    </xsl:template>

    <xsl:template match="rtf:list-text"/>

    <xsl:template name="underline">
        <xsl:choose>
            <xsl:when test="@underlined = 'continuous'">
                <xsl:text>\ul</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'dotted'">
                <xsl:text>\uld</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'double'">
                <xsl:text>\uldb</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'thick'">
                <xsl:text>\ulth</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'thich'">
                <xsl:text>\ulth</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'thick-dotted'">
                <xsl:text>\ulthd</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'dash'">
                <xsl:text>\uldash</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'thick-dash'">
                <xsl:text>\ulthdash</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'long-dash'">
                <xsl:text>\ulthldash</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'dash-dot'">
                <xsl:text>\uldashd</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'dash-dot-dot'">
                <xsl:text>\uldashdd</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'thick-dash-dot-dot'">
                <xsl:text>\ulthdashdd</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'wave'">
                <xsl:text>\ulwave</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'heavy-wave'">
                <xsl:text>\ulhwave</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'double-wave'">
                <xsl:text>\ululdbwave</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'word'">
                <xsl:text>\ulw</xsl:text>
            </xsl:when>
            <xsl:when test="@underlined = 'false'">
                <xsl:text>\ulnone</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>\ul</xsl:text>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <xsl:template match = "rtf:field">
        <xsl:choose>
            <xsl:when test = "@type = 'insert-page-number'">
                <xsl:text>{\field</xsl:text>
                <xsl:text>{\*\fldinst { PAGE }}</xsl:text>
                <xsl:text>{\fldrslt}</xsl:text>
                <xsl:text>}</xsl:text>
            </xsl:when>
            <xsl:when test = "@type = 'Advance'">
                <xsl:text>{\field{\*\fldinst { ADVANCE }}{\fldrslt }}</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>









    <xsl:template name="paragraph-border">
        <xsl:choose>
            <xsl:when test="../@border-paragraph-left-line-width">
                <xsl:text>\brdrl\brdrw</xsl:text>
                <xsl:value-of select="round(../@border-paragraph-left-line-width * 20)"/>
                <xsl:choose>
                    <xsl:when test="../@border-paragraph-left-padding">
                        <xsl:text>\brsp</xsl:text>
                        <xsl:value-of select="round(../@border-paragraph-left-padding * 20)"/>
                    </xsl:when>
                </xsl:choose>
                <xsl:if test="../@border-paragraph-left-color">
                    <xsl:variable name="color" select="../@border-paragraph-left-color"/>
                    <xsl:text>\brdrcf</xsl:text>
                    <xsl:value-of select="key('color-numbers', $color)/@num"/>
                </xsl:if>
                <xsl:if test="../@border-paragraph-left-shadowed-border">
                    <xsl:text>\brdrsh</xsl:text>
                </xsl:if>
            </xsl:when>
        </xsl:choose>

        <xsl:choose>
            <xsl:when test="../@border-paragraph-right-line-width">
                <xsl:text>\brdrr\brdrw</xsl:text>
                <xsl:value-of select="round(../@border-paragraph-right-line-width * 20)"/>
                <xsl:choose>
                    <xsl:when test="../@border-paragraph-right-padding">
                        <xsl:text>\brsp</xsl:text>
                        <xsl:value-of select="round(../@border-paragraph-right-padding * 20)"/>
                    </xsl:when>
                </xsl:choose>
                <xsl:if test="../@border-paragraph-right-color">
                    <xsl:variable name="color" select="../@border-paragraph-right-color"/>
                    <xsl:text>\brdrcf</xsl:text>
                    <xsl:value-of select="key('color-numbers', $color)/@num"/>
                </xsl:if>
                <xsl:if test="../@border-paragraph-right-shadowed-border">
                    <xsl:text>\brdrsh</xsl:text>
                </xsl:if>

            </xsl:when>
        </xsl:choose>

        <xsl:choose>
            <xsl:when test="../@border-paragraph-top-line-width">
                <xsl:text>\brdrt\brdrw</xsl:text>
                <xsl:value-of select="round(../@border-paragraph-top-line-width * 20)"/>
                <xsl:choose>
                    <xsl:when test="../@border-paragraph-top-padding">
                        <xsl:text>\brsp</xsl:text>
                        <xsl:value-of select="round(../@border-paragraph-top-padding * 20)"/>
                    </xsl:when>
                </xsl:choose>
                <xsl:if test="../@border-paragraph-top-color">
                    <xsl:variable name="color" select="../@border-paragraph-top-color"/>
                    <xsl:text>\brdrcf</xsl:text>
                    <xsl:value-of select="key('color-numbers', $color)/@num"/>
                </xsl:if>
                <xsl:if test="../@border-paragraph-top-shadowed-border">
                    <xsl:text>\brdrsh</xsl:text>
                </xsl:if>
            </xsl:when>

        </xsl:choose>
        <xsl:choose>
            <xsl:when test="../@border-paragraph-bottom-line-width">
                <xsl:text>\brdrb\brdrw</xsl:text>
                <xsl:value-of select="round(../@border-paragraph-bottom-line-width * 20)"/>
                <xsl:choose>
                    <xsl:when test="../@border-paragraph-bottom-padding">
                        <xsl:text>\brsp</xsl:text>
                        <xsl:value-of select="round(../@border-paragraph-bottom-padding * 20)"/>
                    </xsl:when>
                </xsl:choose>
                <xsl:if test="../@border-paragraph-bottom-color">
                    <xsl:variable name="color" select="../@border-paragraph-bottom-color"/>
                    <xsl:text>\brdrcf</xsl:text>
                    <xsl:value-of select="key('color-numbers', $color)/@num"/>
                </xsl:if>
                <xsl:if test="../@border-paragraph-bottom-shadowed-border">
                    <xsl:text>\brdrsh</xsl:text>
                </xsl:if>
            </xsl:when>

        </xsl:choose>

    </xsl:template>


    <xsl:template match="table">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="rtf:row">
        <xsl:call-template name="row"/>
        <xsl:apply-templates/>
        <xsl:text>\pard\intbl</xsl:text>
        <xsl:text>{</xsl:text>
        <xsl:call-template name="row"/>
        <xsl:text>\row</xsl:text>
        <xsl:text>}</xsl:text>
    </xsl:template>

    <xsl:template match="rtf:cell">
        <xsl:apply-templates/>
        <xsl:text>\cell </xsl:text>
    </xsl:template>

    <xsl:template name="row">
        <xsl:text>\trowd </xsl:text>

        <!--row borders-->
        <xsl:call-template name="table-row-border"/>
        <xsl:text>&#xA;</xsl:text>

        <!-- Now do cell -->
        <xsl:for-each select="rtf:cell">
            <xsl:call-template name="table-cell-border"/>
            <xsl:call-template name="cell-widths"/>
            <xsl:text>&#xA;</xsl:text>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="cell-widths">
        <xsl:variable name="cell-sum" select="sum(preceding-sibling::rtf:cell/@width)"/>
        <xsl:variable name="current-cell" select="@width"/>
        <xsl:variable name="total-cell" select="($current-cell + $cell-sum) * 20"/>
        <xsl:variable name="row-adjust" select="../@left-row-position * 20"/>
        <xsl:variable name="final-num" select="round($total-cell + $row-adjust)"/>
        <xsl:text>\cellx</xsl:text>
        <xsl:value-of select="$final-num"/>

    </xsl:template>


    <xsl:template name="table-row-border">


        <!--positioning of row-->
        <xsl:if test="@left-row-position">
            <xsl:text>\trleft</xsl:text>
            <xsl:value-of select="round(@left-row-position * 20)"/>
        </xsl:if>

        <!--left border for row-->
        <xsl:choose>
            <xsl:when test="@border-table-row-left-line-width">
                <xsl:text>\trbrdrl\brdrw</xsl:text>
                <xsl:value-of select="round(@border-table-row-left-line-width * 20)"/>
            </xsl:when>
        </xsl:choose>


        <!--right border for row-->
        <xsl:choose>
            <xsl:when test="@border-table-row-right-line-width">
                <xsl:text>\trbrdrr\brdrw</xsl:text>
                <xsl:value-of select="round(@border-table-row-right-line-width * 20)"/>
            </xsl:when>
        </xsl:choose>


        <!--top border for row-->
        <xsl:choose>
            <xsl:when test="@border-table-row-top-line-width">
                <xsl:text>\trbrdrt\brdrw</xsl:text>
                <xsl:value-of select="round(@border-table-row-top-line-width * 20)"/>
            </xsl:when>
        </xsl:choose>


        <!--bottom border for row-->
        <xsl:choose>
            <xsl:when test="@border-table-row-bottom-line-width">
                <xsl:text>\trbrdrb\brdrw</xsl:text>
                <xsl:value-of select="round(@border-table-row-bottom-line-width * 20)"/>
            </xsl:when>
        </xsl:choose>



        <xsl:choose>
            <xsl:when test="@ border-table-row-vertical-inside-line-width">
                <xsl:text>\trbrdrv\brdrw</xsl:text>
                <xsl:value-of select="round(@border-table-row-vertical-inside-line-width * 20)"/>
            </xsl:when>
        </xsl:choose>


        <xsl:choose>
            <xsl:when test="@ border-table-row-horizontal-inside-line-width">
                <xsl:text>\trbrdrh\brdrw</xsl:text>
                <xsl:value-of select="round(@border-table-row-horizontal-inside-line-width * 20)"/>
            </xsl:when>
        </xsl:choose>
        


    </xsl:template>

    <!--parse info in table cells pertaining to borders-->
    <xsl:template name="table-cell-border">



        <!--left border for cell-->
        <xsl:choose>
            <xsl:when test="@border-cell-left-line-width">
                <xsl:text>\clbrdrl\brdrw</xsl:text>
                <xsl:value-of select="round(@border-cell-left-line-width * 20)"/>
                <xsl:if test="@border-cell-left-color">
                    <xsl:variable name="color" select="@border-cell-left-color"/>
                    <xsl:text>\brdrcf</xsl:text>
                    <xsl:value-of select="key('color-numbers', $color)/@num"/>
                </xsl:if>
            </xsl:when>
        </xsl:choose>


        <!--right border for cell-->
        <xsl:choose>
            <xsl:when test="@border-cell-right-line-width">
                <xsl:text>\clbrdrr\brdrw</xsl:text>
                <xsl:value-of select="round(@border-cell-right-line-width * 20)"/>
                <xsl:if test="@border-cell-right-color">
                    <xsl:variable name="color" select="@border-cell-right-color"/>
                    <xsl:text>\brdrcf</xsl:text>
                    <xsl:value-of select="key('color-numbers', $color)/@num"/>
                </xsl:if>
            </xsl:when>
        </xsl:choose>


        <!--top border for cell-->
        <xsl:choose>
            <xsl:when test="@border-cell-top-line-width">
                <xsl:text>\clbrdrt\brdrw</xsl:text>
                <xsl:value-of select="round(@border-cell-top-line-width * 20)"/>
                <xsl:if test="@border-cell-top-color">
                    <xsl:variable name="color" select="@border-cell-top-color"/>
                    <xsl:text>\brdrcf</xsl:text>
                    <xsl:value-of select="key('color-numbers', $color)/@num"/>
                </xsl:if>
            </xsl:when>
        </xsl:choose>


        <!--bottom border for cell-->
        <xsl:choose>
            <xsl:when test="@border-cell-bottom-line-width">
                <xsl:text>\clbrdrb\brdrw</xsl:text>
                <xsl:value-of select="round(@border-cell-bottom-line-width * 20)"/>
                <xsl:if test="@border-cell-bottom-color">
                    <xsl:variable name="color" select="@border-cell-bottom-color"/>
                    <xsl:text>\brdrcf</xsl:text>
                    <xsl:value-of select="key('color-numbers', $color)/@num"/>
                </xsl:if>
            </xsl:when>
        </xsl:choose>


    </xsl:template>


<xsl:template match = "text()">
    <xsl:variable name = "first-string">
        <xsl:call-template name = "replace">
            <xsl:with-param name = "output-string" select = "."/>
            <xsl:with-param name = "target">
                <xsl:text>\</xsl:text>
            </xsl:with-param>
            <xsl:with-param name = "replacement">
                <xsl:text>\\</xsl:text>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:variable>
    <xsl:variable name = "second-string">
        <xsl:call-template name = "replace">
            <xsl:with-param name = "output-string" select = "$first-string"/>
            <xsl:with-param name = "target">
                <xsl:text>{</xsl:text>
            </xsl:with-param>
            <xsl:with-param name = "replacement">
                <xsl:text>\{</xsl:text>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name = "replace">
        <xsl:with-param name = "output-string" select = "$second-string"/>
        <xsl:with-param name = "target">
            <xsl:text>}</xsl:text>
        </xsl:with-param>
        <xsl:with-param name = "replacement">
            <xsl:text>\}</xsl:text>
        </xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template name = "replace">
    <xsl:param name = "output-string"/>
    <xsl:param name = "target"/>
    <xsl:param name = "replacement"/>
    <xsl:choose>
        <xsl:when test = "contains($output-string, $target)">
        <xsl:value-of select = "concat(substring-before($output-string, $target),
        $replacement)"/>
        <xsl:call-template name = "replace">
            <xsl:with-param name = "output-string" select = "substring-after($output-string, $target)"/>
            <xsl:with-param name = "target" select = "$target"/>
            <xsl:with-param name = "replacement" select = "$replacement"/>
        </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select = "$output-string"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>
    

<!--

main elements


-->

</xsl:stylesheet>
