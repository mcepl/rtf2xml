<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:srtf="http://rtf2xml.sourceforge.net/simplertf"
    xmlns:rtf="http://rtf2xml.sourceforge.net/"
    exclude-result-prefixes="rtf srtf"
    version="1.0" 
    >
<xsl:include href = "../common/convert_number.xsl"/>

<xsl:template name = "paragraph-definition">
    <xsl:param name = "list">false</xsl:param>
    <xsl:param name = "level"/>
    <xsl:call-template name = "p-d-name"/>
    <xsl:choose>
        <xsl:when test = "$list = 'true'">
        </xsl:when>
        <xsl:otherwise>
            <xsl:call-template name = "p-d-left-indent"/>
            <xsl:call-template name = "p-d-first-line-indent"/>
        </xsl:otherwise>
    </xsl:choose>
    <xsl:call-template name = "p-d-right-indent"/>
    <xsl:call-template name = "p-d-space-before"/>
    <xsl:call-template name = "p-d-space-after"/>
    <xsl:call-template name = "p-d-line-height"/>
</xsl:template>

<xsl:template name = "paragraph-definition-list">
    
</xsl:template>

<xsl:template name = "p-d-name">
    <xsl:attribute name = "name">
       <xsl:choose>
           <xsl:when test = "ancestor-or-self::*[@style]">
                <xsl:value-of select = "ancestor-or-self::*[@style]/@style"/>
           </xsl:when>
           <xsl:otherwise>
               <xsl:text>xml2rtf-default</xsl:text>
           </xsl:otherwise>
       </xsl:choose> 
    </xsl:attribute>
    <xsl:attribute name = "style-number">
        <xsl:text>s-1</xsl:text>
    </xsl:attribute>
</xsl:template>

<xsl:template name = "p-d-left-indent">
    <xsl:choose>
        <xsl:when test = "@left-indent">
            <xsl:attribute name = "left-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "@left-indent"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "key('para-style', @style)/@left-indent">
            <xsl:attribute name = "left-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "key('para-style', @style)/@left-indent"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "ancestor::*[@left-indent]">
            <xsl:attribute name = "left-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "ancestor::*[@left-indent][1]/@left-indent"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "key('para-style', ancestor::*[@style]/@style)/@left-indent">
            <xsl:attribute name = "left-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "key('para-style', ancestor::*[@style][1]/@style)/@left-indent"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
    </xsl:choose>
</xsl:template>


<!-- For <li> -->
<xsl:template name = "list-left-indent">
    <xsl:param name = "level">1</xsl:param>
    <xsl:param name = "do-first-line">true</xsl:param>
    <!--
    <xsl:attribute name = "testit">
        <xsl:value-of select = "$do-first-line"/>
    </xsl:attribute>
    -->
    <xsl:choose>
        <xsl:when test = "@list-body-indent">
            <xsl:variable name = "this-left-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" 
                        select = "@list-body-indent"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:attribute name = "left-indent">
                <xsl:value-of select = "$this-left-indent"/>
            </xsl:attribute>
            <xsl:attribute name = "list-tabs">
                <xsl:text>left:</xsl:text>
                <xsl:value-of select = "$this-left-indent"/>
                <xsl:text>;</xsl:text>
            </xsl:attribute>
            <xsl:if test = "$do-first-line = 'true'">
                <xsl:call-template name = "list-first-line-indent">
                    <xsl:with-param name = "the-left-indent" select = "$this-left-indent"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:when>
        <xsl:when test = "self::srtf:p and @left-indent">
            <xsl:variable name = "this-left-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" 
                        select = "@left-indent"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:attribute name = "left-indent">
                <xsl:value-of select = "$this-left-indent"/>
            </xsl:attribute>
            <xsl:attribute name = "list-tabs">
                <xsl:text>left:</xsl:text>
                <xsl:value-of select = "$this-left-indent"/>
                <xsl:text>;</xsl:text>
            </xsl:attribute>
            <xsl:if test = "$do-first-line = 'true'">
                <xsl:call-template name = "list-first-line-indent">
                    <xsl:with-param name = "the-left-indent" select = "$this-left-indent"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:when>
        <xsl:when test = "name() = 'srtf:p' and ancestor::srtf:li[1][@list-body-indent]">
            <xsl:variable name = "this-left-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" 
                        select = "ancestor::srtf:li[1]/@list-body-indent"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:attribute name = "left-indent">
                <xsl:value-of select = "$this-left-indent"/>
            </xsl:attribute>
            <xsl:attribute name = "list-tabs">
                <xsl:text>left:</xsl:text>
                <xsl:value-of select = "$this-left-indent"/>
                <xsl:text>;</xsl:text>
            </xsl:attribute>
            <xsl:if test = "$do-first-line = 'true'">
                <xsl:call-template name = "list-first-line-indent">
                    <xsl:with-param name = "the-left-indent" select = "$this-left-indent"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:when>
        <xsl:when test = "ancestor::srtf:ol[1][@list-body-indent]|ancestor::srtf:ul[1][@list-body-indent]">
            <xsl:variable name = "this-left-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" 
                        select = "ancestor::srtf:ol[1]/@list-body-indent|ancestor::srtf:ul[1]/@list-body-indent"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:attribute name = "left-indent">
                <xsl:value-of select = "$this-left-indent"/>
            </xsl:attribute>
            <xsl:attribute name = "list-tabs">
                <xsl:text>left:</xsl:text>
                <xsl:value-of select = "$this-left-indent"/>
                <xsl:text>;</xsl:text>
            </xsl:attribute>
            <xsl:if test = "$do-first-line = 'true'">
                <xsl:call-template name = "list-first-line-indent">
                    <xsl:with-param name = "the-left-indent" 
                    select = "$this-left-indent"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:when>
        <xsl:otherwise>
            <xsl:variable name = "this-left-indent">
                <xsl:value-of select = "($level * $default-list-indent) "/>
            </xsl:variable>
            <xsl:attribute name = "left-indent">
                <xsl:value-of select = "$this-left-indent"/>
            </xsl:attribute>
            <xsl:attribute name = "list-tabs">
                <xsl:text>left:</xsl:text>
                <xsl:value-of select = "$this-left-indent"/>
                <xsl:text>;</xsl:text>
            </xsl:attribute>
            <xsl:if test = "$do-first-line = 'true'">
                <xsl:call-template name = "list-first-line-indent">
                    <xsl:with-param name = "the-left-indent" 
                    select = "$this-left-indent"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>


<xsl:template name = "list-first-line-indent">
    <xsl:param name = "level">1</xsl:param>
    <xsl:param name = "the-left-indent"/>
    <xsl:variable name = "left-indent">
        <xsl:value-of select = "$the-left-indent"/>
    </xsl:variable>
    <xsl:choose>
        <xsl:when test = "@list-label-indent">
            <xsl:attribute name = "first-line-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" 
                        select = "-1 * ($left-indent - @list-label-indent)"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "name() = 'srtf:p' and @list-label-indent">
            <xsl:attribute name = "first-line-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" 
                        select = "-1 * ($left-indent - @first-line-indent)"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "name() = 'srtf:p' and ancestor::srtf:li[1][@list-label-indent]">
            <xsl:attribute name = "first-line-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" 
                        select = " -1 * ($left-indent - ancestor::srtf:li[1]/@list-label-indent)"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "ancestor::srtf:ol[1][@list-label-indent]|ancestor::srtf:ul[1][@list-label-indent]">
            <xsl:variable name = "temp-fi">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" 
                        select = "ancestor::srtf:ol[1]/@list-label-indent|ancestor::srtf:ul[1]/@list-label-indent"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:attribute name = "first-line-indent">
                <xsl:value-of select = "-1 * ($left-indent - $temp-fi)"/>
            </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
            <xsl:attribute name = "first-line-indent">
                <xsl:value-of select = "-1 * $default-list-first-indent"/>
            </xsl:attribute>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>


<xsl:template name = "p-d-right-indent">
    <xsl:choose>
        <xsl:when test = "@right-indent">
            <xsl:attribute name = "right-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "@right-indent"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "key('para-style', @style)/@right-indent">
            <xsl:attribute name = "right-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "key('para-style', @style)/@right-indent"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "ancestor::*[@right-indent]">
            <xsl:attribute name = "right-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "ancestor::*[@right-indent][1]/@right-indent"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "key('para-style', ancestor::*[@style]/@style)/@right-indent">
                <xsl:attribute name = "right-indent">
                    <xsl:call-template name = "convert-number">
                        <xsl:with-param name = "number" select = "key('para-style', ancestor::*[@style][1]/@style)/@right-indent"/>
                    </xsl:call-template>
                </xsl:attribute>
        </xsl:when>
    </xsl:choose>
</xsl:template>


<xsl:template name = "p-d-first-line-indent">
    <xsl:choose>
        <xsl:when test = "@first-line-indent">
            <xsl:attribute name = "first-line-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "@first-line-indent"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "key('para-style', @style)/@first-line-indent">
            <xsl:attribute name = "first-line-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "key('para-style', @style)/@first-line-indent"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "ancestor::*[@first-line-indent]">
            <xsl:attribute name = "first-line-indent">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "ancestor::*[@first-line-indent][1]/@first-line-indent"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "key('para-style', ancestor::*[@style]/@style)/@first-line-indent">
                <xsl:attribute name = "first-line-indent">
                    <xsl:call-template name = "convert-number">
                        <xsl:with-param name = "number" select = "key('para-style', ancestor::*[@style][1]/@style)/@first-line-indent"/>
                    </xsl:call-template>
                </xsl:attribute>
        </xsl:when>
    </xsl:choose>
</xsl:template>



<xsl:template name = "p-d-space-before">
    <xsl:choose>
        <xsl:when test = "@space-before">
            <xsl:attribute name = "space-before">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "@space-before"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "key('para-style', @style)/@space-before">
            <xsl:attribute name = "space-before">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "key('para-style', @style)/@space-before"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "ancestor::*[@space-before]">
            <xsl:attribute name = "space-before">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "ancestor::*[@space-before][1]/@space-before"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "key('para-style', ancestor::*[@style]/@style)/@space-before">
                <xsl:attribute name = "space-before">
                    <xsl:call-template name = "convert-number">
                        <xsl:with-param name = "number" select = "key('para-style', ancestor::*[@style][1]/@style)/@space-before"/>
                    </xsl:call-template>
                </xsl:attribute>
        </xsl:when>
    </xsl:choose>
</xsl:template>


<xsl:template name = "p-d-space-after">
    <xsl:choose>
        <xsl:when test = "@space-after">
            <xsl:attribute name = "space-after">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "@space-after"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "key('para-style', @style)/@space-after">
            <xsl:attribute name = "space-after">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "key('para-style', @style)/@space-after"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "ancestor::*[@space-after]">
            <xsl:attribute name = "space-after">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "ancestor::*[@space-after][1]/@space-after"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "key('para-style', ancestor::*[@style]/@style)/@space-after">
                <xsl:attribute name = "space-after">
                    <xsl:call-template name = "convert-number">
                        <xsl:with-param name = "number" select = "key('para-style', ancestor::*[@style][1]/@style)/@space-after"/>
                    </xsl:call-template>
                </xsl:attribute>
        </xsl:when>
    </xsl:choose>
</xsl:template>

<xsl:template name = "p-d-line-height">
    <xsl:choose>
        <xsl:when test = "@line-height">
            <xsl:attribute name = "line-spacing">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "@line-height"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "key('para-style', @style)/@line-height">
            <xsl:attribute name = "line-spacing">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "key('para-style', @style)/@line-height"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "ancestor::*[@line-height]">
            <xsl:attribute name = "line-spacing">
                <xsl:call-template name = "convert-number">
                    <xsl:with-param name = "number" select = "ancestor::*[@line-height][1]/@line-height"/>
                </xsl:call-template>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "key('para-style', ancestor::*[@style]/@style)/@line-height">
                <xsl:attribute name = "line-spacing">
                    <xsl:call-template name = "convert-number">
                        <xsl:with-param name = "number" select = "key('para-style', ancestor::*[@style][1]/@style)/@line-height"/>
                    </xsl:call-template>
                </xsl:attribute>
        </xsl:when>
    </xsl:choose>
</xsl:template>


<!-- line-spacing-->

</xsl:stylesheet>
