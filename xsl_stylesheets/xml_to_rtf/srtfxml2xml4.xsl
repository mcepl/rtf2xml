<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:rtf="http://rtf2xml.sourceforge.net/"
    exclude-result-prefixes="rtf"
    version="1.0" 
    >
    <xsl:output 
    method = "xml"
    doctype-system = "/home/paul/cvs/rtf2xml/dtd/rtf2xml.53.dtd"
    
    />

<xsl:key name="list" match="rtf:list-in-table" use="@list-table-id" />
<xsl:key name="override" match="rtf:override-list" use="@list-id" />

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>


    <xsl:template match = "rtf:item">
        <xsl:element name = "item">
            <xsl:call-template name = "make-number"/>
            <xsl:copy-of select = "@*"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xsl:template name = "make-number">
        <xsl:call-template name = "concat-levels">
            <xsl:with-param name = "level" select = "@level"/>
            <xsl:with-param name = "id" select = "@id"/>
        </xsl:call-template>
        
    </xsl:template>

    <xsl:template name = "concat-levels">
        <xsl:param name = "level"/>
        <xsl:param name = "id"/>
        <xsl:variable name = "tb-id">
            <xsl:value-of select = "key('override', $id)/@list-table-id"/>
        </xsl:variable>
        <xsl:attribute name = "final-number">
            <xsl:if test = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@show-level1">
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level1-prefix"/>
                <xsl:choose>
                    <xsl:when test = "$level = '1'">
                       <xsl:value-of select = "@number"/> 
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select = "preceding::rtf:item[@level = '1'][1]/@number"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level1-suffix"/>
            </xsl:if>
            <xsl:if test = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@show-level2">
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level2-prefix"/>
                <xsl:choose>
                    <xsl:when test = "$level = '2'">
                       <xsl:value-of select = "@number"/> 
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select = "preceding::rtf:item[@level = '2'][1]/@number"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level2-suffix"/>
            </xsl:if>
            <xsl:if test = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@show-level3">
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level3-prefix"/>
                <xsl:choose>
                    <xsl:when test = "$level = '3'">
                       <xsl:value-of select = "@number"/> 
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select = "preceding::rtf:item[@level = '3'][1]/@number"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level3-suffix"/>
            </xsl:if>
            <xsl:if test = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@show-level4">
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level4-prefix"/>
                <xsl:choose>
                    <xsl:when test = "$level = '4'">
                       <xsl:value-of select = "@number"/> 
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select = "preceding::rtf:item[@level = '4'][1]/@number"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level4-suffix"/>
            </xsl:if>
            <xsl:if test = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@show-level5">
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level5-prefix"/>
                <xsl:choose>
                    <xsl:when test = "$level = '5'">
                       <xsl:value-of select = "@number"/> 
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select = "preceding::rtf:item[@level = '5'][1]/@number"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level5-suffix"/>
            </xsl:if>
            <xsl:if test = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@show-level6">
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level6-prefix"/>
                <xsl:choose>
                    <xsl:when test = "$level = '6'">
                       <xsl:value-of select = "@number"/> 
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select = "preceding::rtf:item[@level = '6'][1]/@number"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level6-suffix"/>
            </xsl:if>
            <xsl:if test = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@show-level7">
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level7-prefix"/>
                <xsl:choose>
                    <xsl:when test = "$level = '7'">
                       <xsl:value-of select = "@number"/> 
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select = "preceding::rtf:item[@level = '7'][1]/@number"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level7-suffix"/>
            </xsl:if>
            <xsl:if test = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@show-level8">
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level8-prefix"/>
                <xsl:choose>
                    <xsl:when test = "$level = '8'">
                       <xsl:value-of select = "@number"/> 
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select = "preceding::rtf:item[@level = '8'][1]/@number"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level8-suffix"/>
            </xsl:if>
            <xsl:if test = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@show-level9">
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level9-prefix"/>
                <xsl:choose>
                    <xsl:when test = "$level = '9'">
                       <xsl:value-of select = "@number"/> 
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select = "preceding::rtf:item[@level = '9'][1]/@number"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:value-of select = "key('list', $tb-id)/rtf:level-in-table[@level = $level]/@level9-suffix"/>
            </xsl:if>
            <xsl:if test = "key('list', $tb-id)/rtf:level-in-table[@level = $level][@numbering-type = 'bullet']">
                <xsl:text>&#xB7;</xsl:text>
            </xsl:if>
        </xsl:attribute>
    </xsl:template>


</xsl:stylesheet>
