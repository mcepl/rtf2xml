<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:srtf="http://rtf2xml.sourceforge.net/simplertf"
    xmlns:rtf="http://rtf2xml.sourceforge.net/"
    xmlns:doc="http://rtf2xml.sourceforge.net/docs"
    exclude-result-prefixes="rtf srtf"
    version="1.0" 
    >
    <xsl:param name = "no-list-name">none</xsl:param>

<xsl:include href = "../common/dec2hex.xsl"/>
<xsl:key name = "para-style" match = "srtf:paragraph-style" use = "@name"/>
<!--
<xsl:key name = "list-para-ids" match = "srtf:temp-list-para-ids" use = "@name"/>
-->

<!--Don't use this for now
If you include a list id in the styles table, any 
style with that number will be treated as the same
list rather than a separate list
-->
<xsl:template name = "make-list-table">
    <xsl:element name = "list-table">
        <xsl:apply-templates select = "/srtf:doc//srtf:ol|/srtf:doc//srtf:ul" mode = "list-table"/>
    </xsl:element>
</xsl:template>


<xsl:template name = "make-para-list-ids">
    <xsl:element name = "temp-list-para-ids">
        <xsl:apply-templates select = "/srtf:doc//srtf:ol|/srtf:doc//srtf:ul" mode = "para-list-ids"/>
    </xsl:element>
</xsl:template>



<xsl:template match = "srtf:ol|srtf:ul" mode = "list-table">
    <xsl:if test = "not(ancestor::srtf:ul) and not(ancestor::srtf:ol)">
   <xsl:element name = "list-in-table">
        <xsl:attribute name = "list-table-id">
            <xsl:variable name = "temp-id">
                <xsl:choose>
                    <xsl:when test = "key('para-style', @style)[@name]">
                        <!--
                        <xsl:value-of select = "generate-id(key('para-style', @style))"/>
                        -->
                        <xsl:value-of select = "generate-id(.)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select = "generate-id(.)"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name = "temp-id2">
                <xsl:value-of select = "translate($temp-id, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQURSTVWXYZ','')"/>
            </xsl:variable>
            <xsl:value-of select = "$temp-id2"/>
        </xsl:attribute>
        <xsl:call-template name = "make-the-level"/>
        <xsl:apply-templates select = "descendant::srtf:ol|descendant::srtf:ul" mode = "level"/>
   </xsl:element> 
   </xsl:if>
</xsl:template>


<xsl:template name = "list-numbering-type">
    <xsl:param name = "list-number-type"/>
    <xsl:choose>
        <xsl:when test = "name(.) = 'ul'">
            <xsl:text>bullet</xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:choose>
                <xsl:when test = "$list-numbering-type">
                    <xsl:value-of select = "$list-numbering-type"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>arabic</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:otherwise>
    </xsl:choose>
    
</xsl:template>

<xsl:template name = "make-the-level">
    <xsl:param name = "name-exists">false</xsl:param>
        <xsl:element name = "level-in-table">
            <xsl:attribute name = "numbering-type">
                <xsl:choose>
                    <xsl:when test = "key('para-style', @style)/@list-style-type">
                        <xsl:call-template name = "list-numbering-type">
                            <xsl:with-param name = "list-numbering-type" 
                            select = "key('para-style', @style)/@list-style-type"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name = "list-numbering-type">
                            <xsl:with-param name = "list-numbering-type" 
                            select = "@list-style-type"/>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute> 
            <xsl:variable name = "level">
                <xsl:value-of select = "count(ancestor::srtf:ol|ancestor::srtf:ul) + 1"/>
            </xsl:variable>
            <xsl:attribute name = "level">
                <xsl:value-of select = "$level"/>
            </xsl:attribute>
            <xsl:if test = "@bold">
                <xsl:attribute name = "bold">
                    <xsl:text>true</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test = "@italics">
                <xsl:attribute name = "italics">
                    <xsl:text>true</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:choose>
                <xsl:when test = "@list-bullet-type">
                    <xsl:attribute name = "bullet-type">
                        <xsl:value-of select = "@list-bullet-type"/>
                    </xsl:attribute>
                </xsl:when>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test = "@list-body-indent">
                    <xsl:variable name = "temp-li">
                        <xsl:call-template name = "convert-number">
                            <xsl:with-param name = "number" 
                                select = "@list-body-indent"/>
                        </xsl:call-template>
                    </xsl:variable>
                    <xsl:attribute name = "left-indent">
                        <xsl:value-of select = "$temp-li"/>
                    </xsl:attribute>
                    <xsl:call-template name = "first-line-for-level">
                        <xsl:with-param name = "left-indent" select = "$temp-li"/>
                        <xsl:with-param name = "level" select = "$level"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:variable name = "temp-li">
                        <xsl:value-of select = "($level * $default-list-indent) "/>
                    </xsl:variable>
                    <xsl:attribute name = "left-indent">
                        <xsl:value-of select = "$temp-li"/>
                    </xsl:attribute>
                    <xsl:call-template name = "first-line-for-level">
                        <xsl:with-param name = "left-indent" select = "$temp-li"/>
                        <xsl:with-param name = "level" select = "$level"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        <xsl:choose>
                <xsl:when test = "key('para-style', @style)/@list-numbering">
                    <xsl:call-template name = "level-string">
                        <xsl:with-param name = "string" select = "key('para-style', @style)/@list-numbering"/>
                        <xsl:with-param name = "level" select = "$level"/>
                    </xsl:call-template>
                    
                </xsl:when>
                <xsl:when test = "@list-numbering">
                    <xsl:call-template name = "level-string">
                        <xsl:with-param name = "string" select = "@list-numbering"/>
                        <xsl:with-param name = "level" select = "$level"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                   <xsl:call-template name = "level-string-no-string">
                       <xsl:with-param name = "level" select = "$level"/>
                   </xsl:call-template> 
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
    
</xsl:template>

<xsl:template name = "first-line-for-level">
    <xsl:param name = "level"/>
    <xsl:param name = "left-indent"/>
    <xsl:attribute name = "first-line-indent">
        <xsl:choose>
            <xsl:when test = "@list-label-indent">
                <xsl:variable name = "temp-first-line-indent">
                    <xsl:call-template name = "convert-number">
                        <xsl:with-param name = "number" 
                            select = "@list-label-indent"/>
                    </xsl:call-template>
                </xsl:variable>
                    <xsl:value-of select = "-1 * ($left-indent - $temp-first-line-indent)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select = "-1 * $default-list-first-indent"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:attribute>
    
</xsl:template>

<!--
<xsl:template match = "srtf:li/srtf:ol|srtf:li/srtf:ul" mode = "level">
    <xsl:call-template name = "make-the-level"/>
        <xsl:apply-templates select = "srtf:li/srtf:ol|srtf:li/srtf:ul" mode = "level"/>
    
</xsl:template>
-->
<xsl:template match = "srtf:ol|srtf:ul" mode = "level">
    <xsl:call-template name = "make-the-level"/>
        <xsl:apply-templates select = "srtf:ol|srtf:ul" mode = "level"/>
    
</xsl:template>
    <doc:p>
        The level-string parses the attribute that looks like this:
        &lt;ol level-numbering = "1.2.3" &gt;
    </doc:p>

<xsl:template name = "level-string">
   <xsl:param name = "string"/> 
   <xsl:param name = "level"/> 
   <xsl:param name = "last-level">9</xsl:param>
   <xsl:choose>
       <xsl:when test = "not($string = '')">
            <xsl:choose>
                <xsl:when test = "contains($string, '9')">
                    <xsl:if test = "$level &gt; 8">
                        <xsl:attribute name = "show-level9">
                            <xsl:text>true</xsl:text>
                        </xsl:attribute>
                        <xsl:variable name = "after" select = "substring-after($string, '9')"/>
                        <xsl:if test = "not($after = '')">
                            <xsl:attribute name = "level9-suffix">
                                <xsl:value-of select = "$after"/>
                            </xsl:attribute>
                        </xsl:if>
                    </xsl:if>
                    <xsl:variable name = "rest" select = "substring-before($string, '9')"/>
                    <xsl:call-template name = "level-string">
                        <xsl:with-param name = "string" select = "$rest"/>
                        <xsl:with-param name = "level" select = "$level"/>
                        <xsl:with-param name = "last-level" >9</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test = "contains($string, '8')">
                    <xsl:if test = "$level &gt; 7">
                        <xsl:attribute name = "show-level8">
                            <xsl:text>true</xsl:text>
                        </xsl:attribute>
                        <xsl:variable name = "after" select = "substring-after($string, '8')"/>
                        <xsl:if test = "not($after = '')">
                            <xsl:attribute name = "level8-suffix">
                                <xsl:value-of select = "$after"/>
                            </xsl:attribute>
                        </xsl:if>
                    </xsl:if>
                    <xsl:variable name = "rest" select = "substring-before($string, '8')"/>
                    <xsl:call-template name = "level-string">
                        <xsl:with-param name = "string" select = "$rest"/>
                        <xsl:with-param name = "level" select = "$level"/>
                        <xsl:with-param name = "last-level">8</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test = "contains($string, '7')">
                    <xsl:if test = "$level &gt;6">
                        <xsl:attribute name = "show-level7">
                            <xsl:text>true</xsl:text>
                        </xsl:attribute>
                        <xsl:variable name = "after" select = "substring-after($string, '7')"/>
                        <xsl:if test = "not($after = '')">
                            <xsl:attribute name = "level7-suffix">
                                <xsl:value-of select = "$after"/>
                            </xsl:attribute>
                        </xsl:if>
                    </xsl:if>
                    <xsl:variable name = "rest" select = "substring-before($string, '7')"/>
                    <xsl:call-template name = "level-string">
                        <xsl:with-param name = "string" select = "$rest"/>
                        <xsl:with-param name = "level" select = "$level"/>
                        <xsl:with-param name = "last-level">7</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test = "contains($string, '6')">
                    <xsl:if test = "$level &gt; 5">
                        <xsl:attribute name = "show-level6">
                            <xsl:text>true</xsl:text>
                        </xsl:attribute>
                        <xsl:variable name = "after" select = "substring-after($string, '6')"/>
                        <xsl:if test = "not($after = '')">
                            <xsl:attribute name = "level2-suffix">
                                <xsl:value-of select = "$after"/>
                            </xsl:attribute>
                        </xsl:if>
                    </xsl:if>
                    <xsl:variable name = "rest" select = "substring-before($string, '5')"/>
                    <xsl:call-template name = "level-string">
                        <xsl:with-param name = "string" select = "$rest"/>
                        <xsl:with-param name = "level" select = "$level"/>
                        <xsl:with-param name = "last-level" >5</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test = "contains($string, '4')">
                    <xsl:if test = "$level &gt; 3">
                        <xsl:attribute name = "show-level4">
                            <xsl:text>true</xsl:text>
                        </xsl:attribute>
                        <xsl:variable name = "after" select = "substring-after($string, '4')"/>
                        <xsl:if test = "not($after = '')">
                            <xsl:attribute name = "level4-suffix">
                                <xsl:value-of select = "$after"/>
                            </xsl:attribute>
                        </xsl:if>
                    </xsl:if>
                    <xsl:variable name = "rest" select = "substring-before($string, '4')"/>
                    <xsl:call-template name = "level-string">
                        <xsl:with-param name = "string" select = "$rest"/>
                        <xsl:with-param name = "level" select = "$level"/>
                        <xsl:with-param name = "last-level" >4</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test = "contains($string, '3')">
                    <xsl:if test = "$level &gt; 2">
                        <xsl:attribute name = "show-level3">
                            <xsl:text>true</xsl:text>
                        </xsl:attribute>
                        <xsl:variable name = "after" select = "substring-after($string, '3')"/>
                        <xsl:if test = "not($after = '')">
                            <xsl:attribute name = "level3-suffix">
                                <xsl:value-of select = "$after"/>
                            </xsl:attribute>
                        </xsl:if>
                    </xsl:if>
                    <xsl:variable name = "rest" select = "substring-before($string, '3')"/>
                    <xsl:call-template name = "level-string">
                        <xsl:with-param name = "string" select = "$rest"/>
                        <xsl:with-param name = "level" select = "$level"/>
                        <xsl:with-param name = "last-level" >3</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test = "contains($string, '2')">
                    <xsl:if test = "$level &gt; 1">
                        <xsl:attribute name = "show-level2">
                            <xsl:text>true</xsl:text>
                        </xsl:attribute>
                        <xsl:variable name = "after" select = "substring-after($string, '2')"/>
                        <xsl:if test = "not($after = '')">
                            <xsl:attribute name = "level2-suffix">
                                <xsl:value-of select = "$after"/>
                            </xsl:attribute>
                        </xsl:if>
                    </xsl:if>
                    <xsl:variable name = "rest" select = "substring-before($string, '2')"/>
                    <xsl:call-template name = "level-string">
                        <xsl:with-param name = "string" select = "$rest"/>
                        <xsl:with-param name = "level" select = "$level"/>
                        <xsl:with-param name = "last-level">2</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test = "contains($string, '1')">
                    <xsl:if test = "$level &gt; 0">
                        <xsl:attribute name = "show-level1">
                            <xsl:text>true</xsl:text>
                        </xsl:attribute>
                        <xsl:variable name = "after" select = "substring-after($string, '1')"/>
                        <xsl:if test = "not($after = '')">
                            <xsl:attribute name = "level1-suffix">
                                <xsl:value-of select = "$after"/>
                            </xsl:attribute>
                        </xsl:if>
                    </xsl:if>
                    <xsl:variable name = "rest" select = "substring-before($string, '1')"/>
                    <xsl:call-template name = "level-string">
                        <xsl:with-param name = "string" select = "$rest"/>
                        <xsl:with-param name = "level" select = "$level"/>
                        <xsl:with-param name = "last-level">1</xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name = "leftover-prefix">
                        <xsl:with-param name = "string" select = "$string"/>
                        <xsl:with-param name = "last-level" select = "$last-level"/>
                        <xsl:with-param name = "level" select = "$level"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
           
       </xsl:when>
       <xsl:otherwise>
           
       </xsl:otherwise>
   </xsl:choose>
</xsl:template>

<xsl:template name = "leftover-prefix">
    <xsl:param name = "string"/>
    <xsl:param name = "last-level"/>
    <xsl:param name = "level"/>
    <xsl:choose>
        <xsl:when test = "$last-level = '1' and $level &gt; 0">
            <xsl:attribute name = "level1-prefix">
                <xsl:value-of select = "$string"/>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "$last-level = '2' and $level &gt; 1">
            <xsl:attribute name = "level2-prefix">
                <xsl:value-of select = "$string"/>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "$last-level = '3' and $level &gt; 2">
            <xsl:attribute name = "level3-prefix">
                <xsl:value-of select = "$string"/>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "$last-level = '4' and $level &gt; 3">
            <xsl:attribute name = "level4-prefix">
                <xsl:value-of select = "$string"/>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "$last-level = '5' and $level &gt; 4">
            <xsl:attribute name = "level5-prefix">
                <xsl:value-of select = "$string"/>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "$last-level = '6' and $level &gt; 5">
            <xsl:attribute name = "level6-prefix">
                <xsl:value-of select = "$string"/>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "$last-level = '7' and $level &gt; 6">
            <xsl:attribute name = "level7-prefix">
                <xsl:value-of select = "$string"/>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "$last-level = '8' and $level &gt; 7">
            <xsl:attribute name = "level8-prefix">
                <xsl:value-of select = "$string"/>
            </xsl:attribute>
        </xsl:when>
        <xsl:when test = "$last-level = '9' and $level &gt; 8">
            <xsl:attribute name = "level9-prefix">
                <xsl:value-of select = "$string"/>
            </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
            <xsl:comment>leftover prefix is: "<xsl:value-of select = "$string"/>"</xsl:comment>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>


<xsl:template name = "level-string-no-string">
    <xsl:param name = "level"/>
    <xsl:if test = "name(.) = 'ol'">
        <xsl:choose>
            <xsl:when test = "$level = '1'">
                <xsl:attribute name = "show-level1">
                    <xsl:text>true</xsl:text>
               </xsl:attribute> 
                <xsl:attribute name = "level1-suffix">
                    <xsl:text>.</xsl:text>
               </xsl:attribute> 
            </xsl:when>
            <xsl:when test = "$level = '2'">
                <xsl:attribute name = "show-level2">
                    <xsl:text>true</xsl:text>
               </xsl:attribute> 
                <xsl:attribute name = "level2-suffix">
                    <xsl:text>.</xsl:text>
               </xsl:attribute> 
            </xsl:when>
            <xsl:when test = "$level = '3'">
                <xsl:attribute name = "show-level3">
                    <xsl:text>true</xsl:text>
               </xsl:attribute> 
                <xsl:attribute name = "level3-suffix">
                    <xsl:text>.</xsl:text>
               </xsl:attribute> 
            </xsl:when>
            <xsl:when test = "$level = '4'">
                <xsl:attribute name = "show-level4">
                    <xsl:text>true</xsl:text>
               </xsl:attribute> 
                <xsl:attribute name = "level4-suffix">
                    <xsl:text>.</xsl:text>
               </xsl:attribute> 
            </xsl:when>
            <xsl:when test = "$level = '5'">
                <xsl:attribute name = "show-level5">
                    <xsl:text>true</xsl:text>
               </xsl:attribute> 
                <xsl:attribute name = "level5-suffix">
                    <xsl:text>.</xsl:text>
               </xsl:attribute> 
            </xsl:when>
            <xsl:when test = "$level = '6'">
                <xsl:attribute name = "show-level6">
                    <xsl:text>true</xsl:text>
               </xsl:attribute> 
                <xsl:attribute name = "level6-suffix">
                    <xsl:text>.</xsl:text>
               </xsl:attribute> 
            </xsl:when>
            <xsl:when test = "$level = '7'">
                <xsl:attribute name = "show-level7">
                    <xsl:text>true</xsl:text>
               </xsl:attribute> 
                <xsl:attribute name = "level7-suffix">
                    <xsl:text>.</xsl:text>
               </xsl:attribute> 
            </xsl:when>
            <xsl:when test = "$level = '8'">
                <xsl:attribute name = "show-level8">
                    <xsl:text>true</xsl:text>
               </xsl:attribute> 
                <xsl:attribute name = "level8-suffix">
                    <xsl:text>.</xsl:text>
               </xsl:attribute> 
            </xsl:when>
            <xsl:when test = "$level = '9'">
                <xsl:attribute name = "show-level9">
                    <xsl:text>true</xsl:text>
               </xsl:attribute> 
                <xsl:attribute name = "level9-suffix">
                    <xsl:text>.</xsl:text>
               </xsl:attribute> 
            </xsl:when>
            <xsl:otherwise>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:if>

</xsl:template>


<xsl:template name = "num-of-show-levels">
    <xsl:param name = "level"/>
    <xsl:variable name = "string">
        <xsl:if test = "@level0-suffix and $level &gt; 0">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@level1-suffix and $level &gt; 1">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@level2-suffix and $level &gt; 2">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@show-level3">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@show-level4">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@show-level5">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@show-level6">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@show-level7">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "@show-level8">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:if test = "not(@list-style-type = 'none')">
            <xsl:text>1</xsl:text>
        </xsl:if>
        <xsl:value-of select = "@level0-suffix"/>
        <xsl:value-of select = "@suffix"/>
        <xsl:value-of select = "@prefix"/>
    </xsl:variable>
    <xsl:value-of select = "string-length($string)"/>

</xsl:template>


<xsl:template name = "which-show-levels">

    <xsl:param name = "level"/>
    <xsl:if test = "@level0-suffix and $level &gt; 0">
        <xsl:attribute name = "show-level0">
            <xsl:text>true</xsl:text>
        </xsl:attribute>
    </xsl:if>
    <xsl:if test = "@level1-suffix and $level &gt; 1">
        <xsl:attribute name = "show-level1">
            <xsl:text>true</xsl:text>
        </xsl:attribute>
    </xsl:if>
    <xsl:if test = "@level2-suffix and $level &gt; 2">
        <xsl:attribute name = "show-level2">
            <xsl:text>true</xsl:text>
        </xsl:attribute>
    </xsl:if>
    <xsl:if test = "not(@list-style-type = 'none')">
        <xsl:choose>
            <xsl:when test = "$level = '0'">
                <xsl:attribute name = "show-level0">
                    <xsl:text>true</xsl:text>
                </xsl:attribute>
            </xsl:when>
            <xsl:when test = "$level = '1'">
                <xsl:attribute name = "show-level1">
                    <xsl:text>true</xsl:text>
                </xsl:attribute>
            </xsl:when>
            <xsl:when test = "$level = '2'">
                <xsl:attribute name = "show-level2">
                    <xsl:text>true</xsl:text>
                </xsl:attribute>
            </xsl:when>
            <xsl:when test = "$level = '3'">
                <xsl:attribute name = "show-level3">
                    <xsl:text>true</xsl:text>
                </xsl:attribute>
            </xsl:when>
        </xsl:choose>
        </xsl:if>

</xsl:template>


<xsl:template name = "make-the-level-numbers">
    <xsl:param name = "level"/>

    <xsl:attribute name = "level-numers">
    <xsl:call-template name = "recursive-level-numbers">
        <xsl:with-param name = "level" select = "$level"/>
        
    </xsl:call-template>
    </xsl:attribute>
</xsl:template>

<xsl:template name = "recursive-level-numbers">
    <xsl:param name = "level"/>
    <xsl:param name = "previous-string">0</xsl:param>
    <xsl:param name = "real-string"/>
    <xsl:param name = "which-att">0</xsl:param>
    <xsl:choose>
        <xsl:when test = "$which-att = '8'">
            <xsl:value-of select = "$real-string"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:variable name = "this-string">
            <xsl:choose>
                <xsl:when test = "$which-att = '0'">
                    <xsl:if test = "@level0-suffix and $level &gt; 0">
                        <xsl:text>\&#x0027;</xsl:text>
                        <xsl:call-template name = "dec-to-hex">
                            <xsl:with-param name = "value" select = "string-length(@level0-prefix) + 1"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:when>
                <xsl:when test = "$which-att = '1'">
                    <xsl:if test = "@level1-suffix and $level &gt; 1">
                        <xsl:text>\&#x0027;</xsl:text>
                        <xsl:variable name = "len">
                            <xsl:value-of select = "string-length(@level1-prefix) + string-length(@level0-suffix) + 
                              $previous-string + 1"/>
                        </xsl:variable>
                        <xsl:call-template name = "dec-to-hex">
                            <xsl:with-param name = "value" select = "$len"/>
                        </xsl:call-template>
                    </xsl:if>
                    
                </xsl:when>
            </xsl:choose>
            </xsl:variable>
            <xsl:variable name = "length-of">
            <xsl:choose>
                <xsl:when test = "$which-att = '0'">
                    <xsl:if test = "@level0-suffix and $level &gt; 0">
                        <xsl:value-of select = "string-length(@level0-prefix) + 1"/>
                    </xsl:if>
                </xsl:when>
                <xsl:when test = "$which-att = '1'">
                    <xsl:if test = "@level1-suffix and $level &gt; 1">
                        <xsl:value-of select = "string-length(@level1-prefix) + string-length(@level0-suffix) + 1"/>
                    </xsl:if>
                </xsl:when>
            </xsl:choose>
            </xsl:variable>
            <xsl:call-template name = "recursive-level-numbers">
                <xsl:with-param name = "level" select = "$level"/>
                <xsl:with-param name = "real-string">
                    <xsl:value-of select = "$real-string"/>
                    <xsl:value-of select = "$this-string"/>
                </xsl:with-param>
                <xsl:with-param name = "previous-string">
                    <xsl:value-of select = "$length-of + $previous-string"/>
                </xsl:with-param>
                <xsl:with-param name = "which-att" select = "$which-att + 1"/>
            </xsl:call-template>
        </xsl:otherwise>
    </xsl:choose>
    
</xsl:template>


<xsl:template name = "length-number-text">
    <xsl:variable name = "string">
        <xsl:if test = "@level0-prefix">
            <xsl:text>1</xsl:text>
        </xsl:if>
    </xsl:variable>
    <xsl:value-of select = "string-length($string)"/>

</xsl:template>



<!--lists-->

<xsl:template match = "srtf:ol|srtf:ul" >
   <xsl:element name = "list">
        <xsl:if test = "not(ancestor::srtf:ol) and not(ancestor::srtf:ul)">
            <xsl:attribute name = "list-template-id">
            <xsl:variable name = "temp-id">
                <xsl:choose>
                    <xsl:when test = "key('para-style', @style)[@name]">
                        <!--
                        <xsl:value-of select = "generate-id(key('para-style', @style))"/>
                        -->
                        <xsl:value-of select = "generate-id(.)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select = "generate-id(.)"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
                <xsl:variable name = "temp-id2">
                    <xsl:value-of select = "translate($temp-id, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQURSTVWXYZ','')"/>
                </xsl:variable>
                <xsl:value-of select = "$temp-id2"/>
            </xsl:attribute>
            <xsl:attribute name = "list-id">
                <xsl:value-of select = "count(preceding::srtf:ol|preceding::srtf:ul) + 1"/>
            </xsl:attribute>
        </xsl:if>
        <xsl:attribute name = "list-type">
            <xsl:choose>
                <xsl:when test = "name(.) = 'ol'">
                    <xsl:text>ordered</xsl:text>
                </xsl:when>
                <xsl:when test = "name(.) = 'ul'">
                    <xsl:text>unordered</xsl:text>
                </xsl:when>
            </xsl:choose>
        </xsl:attribute>
        <xsl:if test = "@list-bullet-type_">
            <xsl:attribute name = "list-bullet-type">
                <xsl:value-of select = "@list-bullet-type"/>
            </xsl:attribute>
        </xsl:if>
        <xsl:attribute name = "level">
            <xsl:value-of select = "count(ancestor::srtf:ol|ancestor::srtf:ul) + 1"/>
        </xsl:attribute>
        <xsl:apply-templates/>
   </xsl:element> 
</xsl:template>



<xsl:template match = "srtf:li">
    <xsl:element name = "item">
        <xsl:choose>
            <xsl:when test = "srtf:p">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name = "paragraph-definition">
                    <xsl:call-template name = "list-left-indent">
                        <xsl:with-param name = "level" select = "count(ancestor::srtf:ol|ancestor::srtf:ul) "/>
                    </xsl:call-template>
                    <xsl:call-template name = "paragraph-definition">
                        <xsl:with-param name = "list">true</xsl:with-param>
                        <xsl:with-param name = "level" select = "count(ancestor::srtf:ol|ancestor::srtf:ul) "/>
                    </xsl:call-template>
                    <xsl:element name = "para">
                        <xsl:apply-templates/>
                    </xsl:element>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:element>
</xsl:template>


<!--end of lists-->


<!--no longer used-->
<xsl:template match = "srtf:ul|srtf:ol" mode = "para-list-ids">
    <xsl:if test = "not(ancestor::srtf:ul) and not(ancestor::srtf:ol)">
        <xsl:if test = "ancestor-or-self::*[@name]">
            <xsl:element name = "temp-list-para-ids">
                <xsl:attribute name = "list-id">
                    <xsl:value-of select = "count(preceding::srtf:ol|preceding::srtf:ul) + 1 "/>
                </xsl:attribute>
                <xsl:attribute name = "name">
                    <xsl:value-of select = "ancestor-or-self::*/@name"/>
                </xsl:attribute>
            </xsl:element> 
        </xsl:if>
    </xsl:if>
</xsl:template>

    
</xsl:stylesheet>
