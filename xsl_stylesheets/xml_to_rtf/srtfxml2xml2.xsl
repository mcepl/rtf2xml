<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:rtf="http://rtf2xml.sourceforge.net/"
    exclude-result-prefixes="rtf"
    version="1.0" 
    >
    <xsl:output 
    method = "xml"
    doctype-system = "/home/paul/cvs/rtf2xml/dtd/rtf2xml.53.dtd"
    
    />

<xsl:key name="font" match="rtf:font-in-table[@name]" use="@name" />
<xsl:key name="level" match="rtf:list-in-table/rtf:level-in-table[@level]" use="@level" />

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match = "rtf:font-table">
        <font-table>
            <xsl:for-each select = 'rtf:font-in-table'>
                
            <xsl:if test = 
    "self::rtf:font-in-table[generate-id(.)=generate-id(key('font', @name))]">
            <xsl:element name = "font-in-table">
                <xsl:attribute name = "name">
                    <xsl:value-of select = "@name"/>
                </xsl:attribute>
                <xsl:attribute name = "num">
                    <xsl:number/>
                </xsl:attribute>
                
            </xsl:element>
            </xsl:if>
            </xsl:for-each>
        </font-table>
    </xsl:template>

    <xsl:template match = "rtf:list-in-table">
        <xsl:if test = "not(preceding-sibling::rtf:list-in-table/@list-table-id = @list-table-id)">
            <xsl:element name = "list-in-table">
            <xsl:if test = "@list-table-id">
                <xsl:attribute name = "list-table-id">
                    <xsl:value-of select = "@list-table-id"/>
                </xsl:attribute>
            </xsl:if>
                
                <xsl:for-each select = "rtf:level-in-table">
                    <xsl:sort select = "@level"/>
                    <xsl:if test = 
                    "not(preceding-sibling::rtf:level-in-table/@level = @level)">
                    <xsl:copy-of select = "."/>
                </xsl:if>
                </xsl:for-each>
                
            </xsl:element>
        </xsl:if>
    </xsl:template>

    <xsl:template match = "rtf:list-table">
        <xsl:element name = "list-table">
            <xsl:apply-templates/>
        </xsl:element>
        <xsl:call-template name = "override-table"/>
    </xsl:template>

    <xsl:template name = "override-table">
        <xsl:element name = "override-table">
            <xsl:for-each select = "/rtf:doc//rtf:list[@list-template-id]">
                <xsl:element name = "override-list">
                    <xsl:attribute name = "list-id">
                        <xsl:value-of select = "@list-id"/>
                    </xsl:attribute>
                    <xsl:attribute name = "list-table-id">
                        <xsl:value-of select = "@list-template-id"/>
                    </xsl:attribute>
                </xsl:element>
            </xsl:for-each>
            
        </xsl:element>
    </xsl:template>

    <xsl:template name = "get-count-id">
        <xsl:param name = "start-count">0</xsl:param>
        <xsl:param name = "id"/>
    </xsl:template>

    <xsl:template match = "rtf:color-in-table">
        <xsl:element name = "color-in-table">
            <xsl:attribute name = "value">
                <xsl:value-of select = "@value"/>
            </xsl:attribute>
            <xsl:attribute name = "num">
                <xsl:number/>
            </xsl:attribute>
        </xsl:element>
    </xsl:template>

    <xsl:template match = "rtf:list">
        <xsl:choose>
            <xsl:when test = "ancestor::rtf:list">
                <xsl:element name = "list">
                    <xsl:attribute name = "list-id">
                        <xsl:value-of select = "ancestor::rtf:list[@list-id]/@list-id"/>
                    </xsl:attribute>
                    <xsl:copy-of select = "@*"/>
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name = "list">
                    <xsl:for-each select = "@*">
                        <xsl:if test = "not(name(.) = 'list-template-id')">
                            <xsl:copy-of select = "."/>
                        </xsl:if>
                    </xsl:for-each>
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match = "rtf:paragraph-styles_">
        <xsl:element name = "paragraph-styles">
            
        </xsl:element>
        
    </xsl:template>

    <xsl:template match = "rtf:paragraph-definition">
        <xsl:element name = "paragraph-definition">
            <xsl:for-each select = "@*">
                <xsl:if test = "not(name() = 'list-tabs')">
                    <xsl:copy-of select = "."/>
                </xsl:if>
            </xsl:for-each>
            <xsl:call-template name = "merge-tabs"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xsl:template name = "merge-tabs">
        <xsl:choose>
            <xsl:when test = "@tabs">
                <xsl:attribute name = "tabs">
                    <xsl:value-of select = "@list-tabs"/>
                    <xsl:value-of select = "@tabs"/>
                </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test = "@list-tabs">
                    <xsl:attribute name = "tabs">
                        <xsl:value-of select = "@list-tabs"/>
                    </xsl:attribute>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>



</xsl:stylesheet>
