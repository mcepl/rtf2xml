import os, re, sys, codecs

import xml.sax.saxutils
import xml.sax
import tempfile
import rtf2xml.copy

# turn on this line if you want to disablenamespaces
from xml.sax.handler import feature_namespaces






class FormSections(xml.sax.saxutils.DefaultHandler):
    """

    Class for handling the XML file. SAX uses the methods in this class to
    handle the data. 


    """
    def __init__(   self,  
                    write_obj,
                    highest_level = 6,
                
                ):

        """
        

        Requires:

            write_obj -- a write object

        Returns:

            nothing


        Logic:

            Set the necessary parameters:

                self.__write_obj --the object for outputing text

                self.__name --name of current element

                self.__character --string containg characters in current
                element.

        """
        self.__write_obj = write_obj
        self.__highest_level = int(highest_level)
        self.__initiate_values()



    def __initiate_values(self):
        """

        Required:

            nothing

        Returns:

            nothing

        Logic:

            initiate values

        """

        self.__levels = []

        # ignore anything inside the list below--don't try to start
        # or end sections
        self.__ignore_list = ['table', 'footnote']
        self.__ignore = ''

        # close out all sections when these elements are reached below
        self.__end_block_list = ['section',  'field-block', 'header-or-footer']

        # stop making secions here
        # self.__start_block_list = ['section', 'field-block', 'header-or-footer']
        self.__state = 'default'
        self.__headings = ['heading 1', 'heading 2', 'heading 3',
        'heading 4', 'heading 5', 'heading 6']
        self.__headings = self.__headings[:(self.__highest_level )]
        self.__level_occurence = []
        num = 0
        for heading in self.__headings:
            self.__level_occurence.append(0)
            num += 1

    
    def startElement(self, name, attrs):
        """

        Logic:

            The SAX driver uses this function when if finds a beginning tag.




        """
        if self.__state == 'default':
            self.__default_start_element(name, attrs)
        elif self.__state == 'ignore':
            self.__write_start_element(name, attrs)
        else:
            sys.stderr.write('Module is transform.headings\n')
            sys.stderr.write('Method is FormSections.startElement\n')
            sys.stderr.write('No state defined here\n')
            self.__write_start_element(name, attrs)


    
    def __default_start_element(self, name, attrs):
        """

        Requires:

            name --name of start element

            attrs -- list of attributes

        Returns:

            Nothing

        Logic:


        """
        if name in self.__ignore_list:
            self.__ignore = name
            self.__write_start_element(name, attrs)
            self.__state = 'ignore'
        elif name in self.__end_block_list:
            self.__close_levels(0)
            self.__write_start_element(name, attrs)
        elif name == 'paragraph-definition':
            par_name = attrs.get('name')
            if par_name in self.__headings:
                self.__handle_level(name, attrs)
            else:
                self.__write_start_element(name, attrs)
        else:
            self.__write_start_element(name, attrs)
        

            

    def __write_start_element(self, name, attrs):

        """

        Write start tag


        """
        open_tag = '<%s' % name
        keys = attrs.keys()
        for key in keys:
            att = key
            value = attrs[key]
            value = self.__escape_characters(value)
            value = value.replace('"', '&quot;')
            open_tag += ' %s="%s"' % (att, value)
        open_tag += '>'
        self.__write_obj.write(open_tag)

    def __handle_level(self, name, attrs):
        """

        """


        par_name = attrs.get('name')
        fields = par_name.split()
        num = fields[1]
        if num:
            num = int(num)
            level = num


        
        if len(self.__levels) > 0:
            last_level = self.__levels[-1]
            if level == last_level:
                self.__close_levels(level)
                self.__add_level(level)
                self.__write_start_element(name, attrs)
            elif level < last_level:
                self.__close_levels(level)
                self.__add_level(level)
                self.__write_start_element(name, attrs)
            elif level > last_level:
                self.__add_level(level)
                self.__write_start_element(name, attrs)
        else:
            self.__add_level(level)
            self.__write_start_element(name, attrs)


    
    def __close_levels(self, level):
        """

        """
        self.__levels.reverse()
        num_of_close = 0
        for previous_level in self.__levels:
            if previous_level >= level:
                num_of_close += 1
                self.__write_obj.write('</section>')
                if previous_level != level:
                    self.__level_occurence[previous_level - 1] = 0
        self.__levels = self.__levels[num_of_close:]
        self.__levels.reverse()


    def __add_level(self, level):
        """


        """
        value =  self.__headings[level - 1]
        self.__level_occurence[level - 1] += 1
        num = self.__level_occurence[level - 1]
        current_level = len(self.__levels) + 1
        value = value.replace('"', '&quot;')
        self.__write_obj.write('<section type="%s"' % value)
        self.__write_obj.write(' num="%s"' % num)
        self.__write_obj.write(' level="%s">' % current_level)
        self.__levels.append(level)


    def characters(self, character):
        """

        Logic:

            The SAX driver uses this function when it finds text.

        """

        character = self.__escape_characters(character)
        self.__write_obj.write(character)
            
    
    def __escape_characters(self, character):
        """

        Method for escaping text

        """
        character = character.replace('&', '&amp;')
        character = character.replace('<', '&lt;')
        character = character.replace('>', '&gt;')
        return character
    
    def __write_end_element(self, name):
        """


        """
        self.__write_obj.write('</%s>' % name)
    
    def endElement(self, name):
        """

        Logic:

            The SAX driver uses the function when it finds an end tag. It
            pases to this function the name of the end element.

        """
        if name == self.__ignore:
            self.__state = 'default'
            self.__ignore = ''
        elif name in self.__end_block_list:
            self.__close_levels(0)
        self.__write_end_element(name)





class Headings:

    def __init__(   self, 
                    in_file, 
                    out_file,
                    highest_level,
                    debug_dir = None,
                    system_dtd = None,
                    ):


        """
        

        Requires:

            file --file to be read

            output --file to output to

        
        Returns:

            Nothing. Outputs a file

        Logic:

            Set up a write object. 

            Create an instance of the InlineHandler for sax to use.

            Pass this instance to the SAX driver.

            Use the SAX driver to handle the file. 


        """
        self.__input_file = in_file
        self.__output_file = out_file
        self.__highest_level = highest_level
        self.__debug_dir = debug_dir
        self.__system_dtd = system_dtd


    def __output_xml(self, in_file, out_file):
        """

        output the ill-formed xml file

        """
            
        (utf8_encode, utf8_decode, utf8_reader, utf8_writer) = codecs.lookup("utf-8")
        write_obj = utf8_writer(open(out_file, 'w'))
        write_obj = open(out_file, 'w')
        read_obj = utf8_writer(open(in_file, 'r'))
        read_obj = open(in_file, 'r')
        line = 1
        while line:
            line = read_obj.readline()
            write_obj.write(line)
        
        read_obj.close()
        write_obj.close()


        

    def make_sections(self):



        temp_output = tempfile.mktemp()
        (utf8_encode, utf8_decode, utf8_reader, utf8_writer) = codecs.lookup("utf-8")
        write_obj = utf8_writer(open(temp_output, 'w'))
        parser = xml.sax.make_parser()
        parser.setFeature(feature_namespaces, 0)
        parser.setFeature("http://xml.org/sax/features/external-general-entities", False)
        section_handler = FormSections( 
            write_obj = write_obj,
            highest_level = self.__highest_level,
            )
        
        parser.setContentHandler(section_handler)

        


    
        # write XML DTD
        write_obj.write('<?xml version="1.0"?>\n')
        if self.__system_dtd:
            write_obj.write('<!DOCTYPE doc SYSTEM "%s">\n' % self.__system_dtd)
        parser.parse(self.__input_file)             
        write_obj.close()


        # output to debug regardless of if it is ill-formed
        if  self.__debug_dir:
            out_file = os.path.join(self.__debug_dir, 'headings.xml')
            self.__output_xml(temp_output, out_file)

        # test file for well-formedness
        well_formed = 0
        parser = xml.sax.make_parser()
        parser.setFeature(feature_namespaces, 0)
        parser.setFeature("http://xml.org/sax/features/external-general-entities",False)
        try:
            parser.parse(temp_output)             
            well_formed = 1
        except xml.sax._exceptions.SAXParseException:
            os.remove(temp_output)
            return 1
        self.__output_xml(temp_output, self.__output_file)


        os.remove(temp_output)
        return 0


            
if __name__ == '__main__':
    if len(sys.argv) != 3:
        sys.stderr.write('Please provide both an input and output file:\n')
        sys.stderr.write('python sax_copy.py output_file input_file\n')
        sys.exit(1)
    input_file = sys.argv[2]
    output_file = sys.argv[1]
    the_debug_dir = '/home/paul/tmp/rtf'
    the_headings_obj = Headings(
        in_file = input_file,
        out_file = output_file,
        highest_level = 6,
        debug_dir = the_debug_dir,
            )
    result = the_headings_obj.make_sections()
    if result == 0:
        print 'File well-formed'
    else:
        print 'Sorry, file not well-formed'

